package eu.dnetlib.data.collector.plugins.ariadneplus.thanados;

import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class ThanadosIteratorTest {

    private String sitelist = "https://thanados.net/sites/sitelist";
    ThanadosIterator it;

    @Test
    public void testDownloadList(){
        it = new ThanadosIterator("", sitelist, "");
        it.getIdentifiers().stream().forEach(id -> System.out.println(id));
        System.out.println(it.getIdentifiers().size());
    }

    @Test
    public void testDownload(){
        int count = 0;
        it = new ThanadosIterator("https://thanados.openatlas.eu/api/0.3/subunits", sitelist, "");
        while(it.hasNext()){
            it.next();
            count++;
            System.out.println(count);
        }
        System.out.println("DONE");
    }


    @Test
    public void testIncremental(){
        it = new ThanadosIterator("", sitelist, "2022-03-12");
        it.getIdentifiers().stream().forEach(id -> System.out.println(id));
        System.out.println(it.getIdentifiers().size());
    }

}
