package eu.dnetlib.data.collector.plugins.ariadneplus.ads;

import com.ximpleware.*;
import com.ximpleware.EOFException;

import java.io.*;
import org.junit.Assert;
import org.junit.Test;

public class PreProcessRecordTest {

	@Test
	public void preprocessXMLRecord2() throws EncodingException, EOFException, EntityException, ParseException, ModifyException, XPathParseException, XPathEvalException, NavException, IOException, TranscodeException {
		
		String namespaceList = " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n" + 
				"    xmlns:dcterms=\"http://purl.org/dc/terms/\"\n" + 
				"    xmlns:ads=\"https://archaeologydataservice.ac.uk/\"";
		
		System.out.println("namespaceList: "+ namespaceList + "\n");
		
		// TODO Auto-generated method stub
        VTDGen vg = new VTDGen();
        String xml="<record> " + 
        		"<dc:title>Part of a cropmark with...</dc:title>\n" + 
        		"<dc:creator>Historic England</dc:creator>\n" + 
        		"<dc:subjectPeriod>\n" + 
        		"    <dc:subject>CROPMARK</dc:subject>\n" + 
        		"    <dcterms:temporal>PALAEOLITHIC</dcterms:temporal>\n" + 
        		"</dc:subjectPeriod>\n" + 
        		"<dc:subjectPeriod>\n" + 
        		"    <dc:subject>HOUSE</dc:subject>\n" + 
        		"</dc:subjectPeriod>\n" + 
        		"<dc:description>Part of a cropmark with internal markings seen on air photograph. Field investigation in 1957 found the marks to be due to natural undulations and vegetational changes, and no traces of antiquity were observed.</dc:description>\n" + 
        		"<dc:identifier>Depositor ID: NT 70 NE 1</dc:identifier>\n" + 
        		"<dc:identifier>NMR_NATINV-3</dc:identifier>\n" + 
        		"<dc:source>https://archaeologydataservice.ac.uk/archsearch/record?titleId=967971</dc:source>\n" + 
        		"<dc:language xsi:type=\"dcterms:ISO639-2\">eng</dc:language>\n" + 
        		"<dcterms:spatial>ALWINTON, ALNWICK, NORTHUMBERLAND, ENGLAND</dcterms:spatial>\n" + 
        		"<dcterms:spatial xsi:type=\"dcterms:POINT\">-2.758696, 55.945225</dcterms:spatial>\n" + 
        		"<dc:rights>http://archaeologydataservice.ac.uk/advice/termsOfUseAndAccess</dc:rights>\n" + 
        		"</record> ";
        System.out.println("original record: "+xml + "\n");
        vg.setDoc(xml.getBytes());
        vg.parse(false); // namespace unaware to all name space nodes addressable using xpath @*
        VTDNav vn = vg.getNav();
        XMLModifier xm = new XMLModifier(vn);
        
        byte[] attrBytes = namespaceList.getBytes();
        
        vn.toElement(VTDNav.ROOT);
        xm.insertAttribute(attrBytes);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        xm.output(baos);
        System.out.println("preprocessed record: " + baos.toString() + "\n");
		Assert.assertNotNull(baos);
	}
}
