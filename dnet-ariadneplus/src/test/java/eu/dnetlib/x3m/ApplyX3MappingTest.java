package eu.dnetlib.x3m;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import eu.dnetlib.msro.workflows.nodes.transform.ApplyX3Mapping;
import gr.forth.ics.isl.x3ml.X3MLEngineFactory;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

public class ApplyX3MappingTest {

	private static final Log log = LogFactory.getLog(ApplyX3MappingTest.class);

	private String mappingUrl = "https://mapping.d4science.org/3MEditor/Services?id=591&output=text/xml&method=export";

	//Ariadne policy
	final String ariadnePolicyPath = "ariadne_policy.xml";

	//ADS record
	final String adsRecordPath = "ariadneADSsample.xml";

	@Test
	public void testAriadne() throws IOException {
		doBasicTest(ariadnePolicyPath, adsRecordPath);
	}

	@Test
	public void testWithURLMapping() throws MalformedURLException {
		X3MLEngineFactory x3mEngine = X3MLEngineFactory.create()
				.withMappings(new URL("https://mapping.d4science.org/3MEditor/Services?id=591&output=text/xml&method=export"))
				.withVerboseLogging()
				.withGeneratorPolicy(getInputStreamFromClasspath("/eu/dnetlib/x3m/ariadne_policy.xml"))
				.withInput(getInputStreamFromClasspath("/eu/dnetlib/x3m/ariadneADSsample.xml"))
				.withOutput(System.out, X3MLEngineFactory.OutputFormat.RDF_XML);
		x3mEngine.execute();
	}

	@Test
	public void testDime() throws MalformedURLException {
		X3MLEngineFactory x3mEngine = X3MLEngineFactory.create()
				.withMappings(new URL("https://mapping.d4science.org/3MEditor/Services?id=697&output=text/xml&method=export"))
				.withVerboseLogging()
				//.withGeneratorPolicy(new URL("https://mapping.d4science.org/3MEditor/FetchBinFile?type=generator_link&file=ARIADNEplusGeneratorPolicy_v1.8___21-04-2021112747___12811.xml"))
				.withGeneratorPolicy(getInputStreamFromClasspath("/eu/dnetlib/x3m/ariadne_policy.xml"))
				.withInput(getInputStreamFromClasspath("/eu/dnetlib/x3m/dime.xml"))
				.withOutput(System.out, X3MLEngineFactory.OutputFormat.RDF_XML);
		x3mEngine.execute();
	}

	@Test
	public void testCarareCollection() throws MalformedURLException {
		X3MLEngineFactory x3mEngine = X3MLEngineFactory.create()
				.withMappings(new URL("https://mapping.d4science.org/3MEditor/Services?id=1010&output=text/xml&method=export"))
				.withVerboseLogging()
				.withGeneratorPolicy(getInputStreamFromClasspath("/eu/dnetlib/x3m/ariadne_policy.xml"))
				.withInput(getInputStreamFromClasspath("/eu/dnetlib/x3m/carare_collection.xml"))
				.withOutput(System.out, X3MLEngineFactory.OutputFormat.RDF_XML);
		x3mEngine.execute();
	}



	public void doBasicTest(String policyPath, String recordPath) throws IOException {
		String g = getString(policyPath);
		String r = getString(recordPath);
		URL mappingURL = new URL(mappingUrl);
		ApplyX3Mapping x3m = new ApplyX3Mapping(mappingURL, g, true);
		String res = x3m.apply(r);
		System.out.println("res is \n"+res);
	}

	public static InputStream getInputStreamFromClasspath(final String s) {
		try {
			final ClassPathResource resource = new ClassPathResource(s);
			return resource.getInputStream();
		}catch(IOException e){
			return null;
		}
	}

	private String getString(final String classpath) throws IOException {
		final ClassPathResource resource = new ClassPathResource(classpath);
		return IOUtils.toString(resource.getInputStream(), "UTF-8");
	}

}