package eu.dnetlib.ariadneplus.workflows.nodes;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Ignore;
import org.junit.Test;

import java.io.InputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.List;

public class RestClientTest {

//    @Test
    @Ignore
    public void getVersion() throws Exception {
        int statusCode = -1;
        String response = "";
        String endpoint = "https://aggregator.ariadne.d4science.org/ariadneplus-graphdb/version";
        System.out.println("getVersion endpoint: " + endpoint);
        HttpClient client = null;
        try {

            URI getURI = new URIBuilder(endpoint)
                    .build();
            client = HttpClients.createDefault();
            HttpResponse res = client.execute(new HttpGet(getURI));
            HttpEntity entity = res.getEntity();
            String version = EntityUtils.toString(entity);
            if (res.getStatusLine()!=null) {
                statusCode = res.getStatusLine().getStatusCode();
            }
            System.out.println("statusCode: "+statusCode);
            System.out.println("version: "+version);
        }
        catch (Throwable t) {
            t.printStackTrace();
        }
    }

//    @Test
    @Ignore
    public void selectIdentifiersTest() throws Exception {
        String datasource = "ads";
        String collectionId = "398";
        String resourceType = "";

        resourceType = "COLLECTION";
        selectIdentifiersTest(datasource, collectionId, resourceType);

        resourceType = "RECORD";
        selectIdentifiersTest(datasource, collectionId, resourceType);
    }

    private List<String> selectIdentifiersTest(String datasource, String collectionId, String resourceType) throws Exception {
        int statusCode = -1;
        String response = "";
        List<String> identifiers = null;
        String endpoint = "https://aggregator.ariadne.d4science.org/ariadneplus-graphdb/selectIdentifiers";
        System.out.println("selectCollectionIdentifiersTest endpoint: " + endpoint);
        HttpClient client = null;
        try {

            URI getURI = new URIBuilder(endpoint)
                    .addParameter("datasource",  datasource)
                    .addParameter("collectionId",  collectionId)
                    .addParameter("resourceType",  resourceType)
                    .build();
            client = HttpClients.createDefault();
            HttpResponse res = client.execute(new HttpGet(getURI));
            if (res.getStatusLine()!=null) {
                statusCode = res.getStatusLine().getStatusCode();
                System.out.println("statusCode: "+statusCode);
            }
            HttpEntity entity = res.getEntity();
            String content = EntityUtils.toString(entity);
            String[] identifiersStr = content.split(",");
            System.out.println("first decoded: "+identifiersStr[0]);
            identifiers = Arrays.asList(identifiersStr);
            System.out.println("retrieved "+identifiers.size()+ " identifiers");
            if (!identifiers.isEmpty()) {
                System.out.println("first: " + identifiers.get(0));
            }

        }
        catch (Throwable t) {
            t.printStackTrace();
        }
        return identifiers;
    }

    private String indexingTest(String datasource, String collectionId, String resourceType, String identifier) throws Exception {
        int statusCode = -1;
        String response = "";
        String result = "";
        String endpoint = "https://aggregator.ariadne.d4science.org/ariadneplus-graphdb/indexOnESByIdentifier";
        System.out.println("indexingTest endpoint: " + endpoint + " datasource: "+datasource+ " collectionId: "+collectionId+ " identifier: "+identifier);
        HttpClient client = null;
        try {
            URI postURI = new URIBuilder(endpoint)
                    .addParameter("datasource",  datasource)
                    .addParameter("collectionId",  collectionId)
                    .addParameter("resourceType",  resourceType)
                    .addParameter("identifier", identifier)
                    .build();
            client = HttpClients.createDefault();
            HttpResponse res = client.execute(new HttpPost(postURI));
            if (res.getStatusLine()!=null) {
                statusCode = res.getStatusLine().getStatusCode();
                System.out.println("statusCode: "+statusCode);
            }
            HttpEntity entity = res.getEntity();
            result = EntityUtils.toString(entity);
            System.out.println("result: " + result);

        }
        catch (Throwable t) {
            t.printStackTrace();
        }
        return result;
    }

//    @Test
    @Ignore
    public void indexOnESByIdentifierTest() throws Exception {
        String datasource = "ads";
        String collectionId = "801";
        String resourceType = "";

        resourceType = "COLLECTION";
        List<String> collectionIdentifiers = selectIdentifiersTest(datasource, collectionId, resourceType);
        if (!collectionIdentifiers.isEmpty()) {
            indexingTest(datasource, collectionId, resourceType, cleanIdentifier(collectionIdentifiers.get(0)));
        }

        resourceType = "RECORD";
        List<String> recordIdentifiers = selectIdentifiersTest(datasource, collectionId, resourceType);
        if (!recordIdentifiers.isEmpty()) {
            indexingTest(datasource, collectionId, resourceType, cleanIdentifier(recordIdentifiers.get(0)));
        }
    }

//        @Test
    @Ignore
    public void selectIdentifierTest() throws Exception {
        String datasource = "ads";
        String collectionId = "801";
        String resourceType = "";

        resourceType = "COLLECTION";
        List<String> identifiers = selectIdentifiersTest(datasource, collectionId, resourceType);
        System.out.println("identifiers: " + cleanIdentifier(identifiers.get(0)));
    }

    private String cleanIdentifier(String identifier) {
        System.out.println("cleaning: "+identifier);
        String cleaned = identifier;
        try {
            cleaned = identifier
                    .replace("[", "")
                    .replace("]", "")
                    .replace("\"", "");
        }
        catch (Exception e) {

        }
        return cleaned;
    }
}