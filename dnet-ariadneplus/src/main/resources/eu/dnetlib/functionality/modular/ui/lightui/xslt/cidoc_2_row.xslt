<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
                xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
                xmlns:dr="http://www.driver-repository.eu/namespace/dr"
                xmlns:dri="http://www.driver-repository.eu/namespace/dri"
                xmlns:dc="http://purl.org/dc/elements/1.1/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
                xmlns:foaf="http://xmlns.com/foaf/0.1/"
		xmlns:aocat="https://www.ariadne-infrastructure.eu/resource/ao/cat/1.0/">

	<xsl:output method="html" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<xsl:variable name="id"   select="//*[local-name() = 'objIdentifier']" />
			<div class="well">
			<table>
				<tr>
					<td style="width: 120px">
						<img src="../resources/img/record.png" width="80" height="80" />
					</td>
					<td>
						<strong>D-Net identifier: </strong><i><xsl:value-of select="//dri:objIdentifier" /></i><br/>
						<strong>Original identifier: </strong><i><xsl:value-of select="//dri:recordIdentifier" /></i><br/>
						<strong>Data Source: </strong><i><xsl:value-of select="//dri:datasourcename" /></i><br/>
						<strong>Data Source API: </strong><i><xsl:value-of select="//dri:datasourceapi" /></i><br/>
						<strong>Resource types: </strong>
						<xsl:for-each-group select="//rdf:Description" group-by="./rdf:type/@rdf:resource">
							<i><xsl:value-of select="tokenize(current-grouping-key(), '/')[last()]"
							/></i> (<xsl:value-of select="count(current-group())"/>) --
						</xsl:for-each-group>
						<br />
						<strong>Titles: </strong><xsl:for-each select="//aocat:has_title"><i><xsl:value-of select="." /></i><br/></xsl:for-each><br/>
						<br />
						<button class="btn btn-primary btn-sm" ng-click="getDocument('objidentifier', '{$id}')">show record</button>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>

</xsl:stylesheet>