<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
                xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
                xmlns:crmpe="http://ariadneplus.d4science.org/CRMext/CRMpe.rdfs/"
                xmlns:crmdig="http://www.ics.forth.gr/isl/CRMext/CRMdig.rdfs/"
                xmlns:dr="http://www.driver-repository.eu/namespace/dr"
                xmlns:dri="http://www.driver-repository.eu/namespace/dri"
                xmlns:dc="http://purl.org/dc/elements/1.1/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:foaf="http://xmlns.com/foaf/0.1/">

	<xsl:output method="html" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<div class="row">
			<div class="col-xs-2">
				<img src="../resources/img/record.png" width="80" height="80"/>
			</div>
			<div class="col-xs-10">
				<xsl:if test="//*[local-name() = 'title']">
					<h1 id="overview">
						<!-- this is visualized big next to the picture. Since we cannot identify 1 main title, for AriadnePlus it wil be empty -->
						<!--<xsl:value-of select="/crm:P102_has_title"/>-->
					</h1>
				</xsl:if>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<h6><span class="glyphicon glyphicon-align-left"/> GENERAL INFORMATION </h6>
				<table class="table">
					<tbody>
						<tr>
							<td class="col-xs-3">
								<strong>Original identifier</strong>
							</td>
							<td><xsl:value-of select="//dri:recordIdentifier"/></td>
						</tr>
						<tr>
							<td class="col-xs-3">
								<strong>Provenance</strong>
							</td>
							<td>
								<i>Data source: </i>
								<xsl:value-of select="//dri:datasourcename"/>
								<br/>
								<i>D-Net API: </i>
								<xsl:value-of select="//dri:datasourceapi"/>
							</td>
						</tr>
						<tr>
							<td class="col-xs-3">
								<strong>Collected</strong>
							</td>
							<td>
								<xsl:value-of select="//dri:dateOfCollection"/>
							</td>
						</tr>
						<tr>
							<td class="col-xs-3">
								<strong>Transformed</strong>
							</td>
							<td>
								<xsl:value-of select="//dri:dateOfTransformation"/>
							</td>
						</tr>
						<tr>
							<td class="col-xs-3">
								<strong>Resource types</strong>
							</td>
							<td>
								<xsl:for-each-group select="//rdf:Description"
								                    group-by="./rdf:type/@rdf:resource">
									<i><xsl:value-of
											select="tokenize(current-grouping-key(), '/')[last()]"
									/></i> (<xsl:value-of select="count(current-group())"/>) --
								</xsl:for-each-group>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<xsl:for-each select="//*[local-name() = 'record']">
			<h6><span class="glyphicon glyphicon-wrench"/> INDEXED RECORD </h6>
			<xsl:call-template name="xmlItem">
				<xsl:with-param name="indent" select="string('')"/>
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="xmlAttr">
		<span style="color:orange">
			<xsl:value-of select="concat(' ', local-name())"/>
		</span>
		<xsl:value-of select="concat('=&quot;', ., '&quot;')"/>
	</xsl:template>


	<xsl:template name="xmlItem">
		<xsl:param name="indent"/>
		<xsl:variable name="tag" select="name()"/>
		<xsl:variable name="newindent"><xsl:value-of select="$indent"/>&#160;&#160;&#160;&#160; </xsl:variable>

		<xsl:if test="string-length($tag)">
			<br/>
			<xsl:value-of select="$indent"/>
			<span style="color:blue">
				<xsl:value-of select="concat('&lt;', $tag)"/>
			</span>
			<xsl:for-each select="@*">
				<xsl:call-template name="xmlAttr"/>
			</xsl:for-each>
			<xsl:choose>
				<xsl:when test="count(child::*) = 0 and count(child::text()) = 1">
					<span style="color:blue">&gt;</span>
					<xsl:value-of select="normalize-space(.)"/>
					<span style="color:blue">
						<xsl:value-of select="concat('&lt;/', $tag, '&gt;')"/>
					</span>
				</xsl:when>
				<xsl:when test="count(child::* | child::text()) &gt; 0">
					<span style="color:blue">&gt;</span>
					<xsl:for-each select="child::* | child::text()">
						<xsl:choose>
							<xsl:when
									test="self::text() and string-length(normalize-space(.)) &gt; 0">
								<xsl:value-of select="normalize-space(.)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="xmlItem">
									<xsl:with-param name="indent" select="$newindent"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
					<br/>
					<xsl:value-of select="$indent"/>
					<span style="color:blue">
						<xsl:value-of select="concat('&lt;/', $tag, '&gt;')"/>
					</span>
				</xsl:when>
				<xsl:otherwise>
					<span style="color:blue">&#160;/&gt;</span>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>

	<!--
        Suppress warning messages "The source document is in no namespace, but the template rules
        all expect elements in a namespace" (see https://github.com/daisy/pipeline-mod-braille/issues/38)
    -->
	<xsl:template match="/phony">
		<xsl:next-match/>
	</xsl:template>


</xsl:stylesheet>
