<?xml version="1.0" encoding="UTF-8"?>
<RESOURCE_PROFILE>
    <HEADER>
        <RESOURCE_IDENTIFIER value=""/>
        <RESOURCE_TYPE value="WorkflowDSResourceType"/>
        <RESOURCE_KIND value="WorkflowDSResources"/>
        <RESOURCE_URI value=""/>
        <DATE_OF_CREATION value=""/>
    </HEADER>
    <BODY>
        <WORKFLOW_NAME>$name$</WORKFLOW_NAME>
        <WORKFLOW_DESCRIPTION>$desc$</WORKFLOW_DESCRIPTION>
        <WORKFLOW_INFO />
        <WORKFLOW_FAMILY>publishing</WORKFLOW_FAMILY>
        <WORKFLOW_PRIORITY>$priority$</WORKFLOW_PRIORITY>
        <DATASOURCE id="$dsId$" interface="$interface$" />
        <CONFIGURATION status="WAIT_SYS_SETTINGS" start="MANUAL">
            <PARAMETERS>
                <PARAM name="publisherEndpoint"     description="AriadnePlus Publisher Endpoint"      required="true" managedBy="user"    type="string">http://localhost:8080/ariadneplus/unpublish</PARAM>
            </PARAMETERS>
            <WORKFLOW>
                	<NODE name="unpublishGraphDB" type="UnpublishGraphDB" isStart="true">
                    	<DESCRIPTION>Drop from GraphDB</DESCRIPTION>
                        <PARAMETERS>
                            <PARAM name="publisherEndpoint" ref="publisherEndpoint"/>
                            <PARAM name="datasourceInterface" value="$interface$" />
                        </PARAMETERS>
                        <ARCS>
                            <ARC to="success"/>
                        </ARCS>
                    </NODE>
           </WORKFLOW>
           <DESTROY_WORKFLOW_TEMPLATE id="23ef4bb3-2383-45b4-9661-ab03472fcd52_V29ya2Zsb3dUZW1wbGF0ZURTUmVzb3VyY2VzL1dvcmtmbG93VGVtcGxhdGVEU1Jlc291cmNlVHlwZQ==">
               <PARAMETERS/>
           </DESTROY_WORKFLOW_TEMPLATE></CONFIGURATION>
        <NOTIFICATIONS/>
        
        <SCHEDULING enabled="false">
            <CRON>9 9 9 ? * *</CRON>
            <MININTERVAL>10080</MININTERVAL>
        </SCHEDULING>
        <STATUS/>
    </BODY>
</RESOURCE_PROFILE>
