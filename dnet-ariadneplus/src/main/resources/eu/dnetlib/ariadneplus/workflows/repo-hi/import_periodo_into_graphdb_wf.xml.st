<?xml version="1.0" encoding="UTF-8"?>
<RESOURCE_PROFILE>
    <HEADER>
        <RESOURCE_IDENTIFIER value=""/>
        <RESOURCE_TYPE value="WorkflowDSResourceType"/>
        <RESOURCE_KIND value="WorkflowDSResources"/>
        <RESOURCE_URI value=""/>
        <DATE_OF_CREATION value=""/>
    </HEADER>
    <BODY>
        <WORKFLOW_NAME>$name$</WORKFLOW_NAME>
        <WORKFLOW_DESCRIPTION>$desc$</WORKFLOW_DESCRIPTION>
        <WORKFLOW_INFO />
        <WORKFLOW_FAMILY>aggregator</WORKFLOW_FAMILY>
        <WORKFLOW_PRIORITY>$priority$</WORKFLOW_PRIORITY>
        <DATASOURCE id="$dsId$" interface="$interface$" />

        <CONFIGURATION status="WAIT_SYS_SETTINGS" start="MANUAL">
            <PARAMETERS>
                <PARAM name="publisherEndpoint"     description="AriadnePlus Publisher Endpoint"      required="true" managedBy="user"    type="string">https://aggregator.ariadne.d4science.org/ariadneplus-graphdb</PARAM>
            	<PARAM name="context"     description="GraphDB context related to periodo data"      required="true" managedBy="user"    type="string">ariadneplus::PROVIDER::periodo</PARAM>
            	<PARAM name="dataUrl"     description="Url of Periodo file to import"      required="true" managedBy="user"    type="string"></PARAM>
            </PARAMETERS>
            <WORKFLOW>
                <NODE name="importPeriodoIntoGraphDB" type="LaunchWorkflowTemplate" isStart="true">
                    <DESCRIPTION>Import periodo data into GraphDB from url</DESCRIPTION>
                    <PARAMETERS>
                        <PARAM name="wfTemplateId" value="f797c7b5-9f2e-4ae8-b908-2b910765fc2b_V29ya2Zsb3dUZW1wbGF0ZURTUmVzb3VyY2VzL1dvcmtmbG93VGVtcGxhdGVEU1Jlc291cmNlVHlwZQ=="/>
                        <PARAM name="wfTemplateParams">
                            <MAP>
                                <ENTRY key="publisherEndpoint"  ref="publisherEndpoint" />
                                <ENTRY key="dataUrl"  ref="dataUrl" />
                                <ENTRY key="context"  ref="context" />
                            </MAP>
                        </PARAM>
                    </PARAMETERS>
                    <ARCS>
                        <ARC to="success"/>
                    </ARCS>
                </NODE>
            </WORKFLOW>
            <DESTROY_WORKFLOW_TEMPLATE id="23ef4bb3-2383-45b4-9661-ab03472fcd52_V29ya2Zsb3dUZW1wbGF0ZURTUmVzb3VyY2VzL1dvcmtmbG93VGVtcGxhdGVEU1Jlc291cmNlVHlwZQ==">
            	<PARAMETERS/>
            </DESTROY_WORKFLOW_TEMPLATE>
        </CONFIGURATION>

        <NOTIFICATIONS/>
        
        <SCHEDULING enabled="false">
            <CRON>9 9 9 ? * *</CRON>
            <MININTERVAL>10080</MININTERVAL>
        </SCHEDULING>
        <STATUS/>
    </BODY>
</RESOURCE_PROFILE>
