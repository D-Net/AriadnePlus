<?xml version="1.0" encoding="UTF-8"?>
<RESOURCE_PROFILE>
    <HEADER>
        <RESOURCE_IDENTIFIER value=""/>
        <RESOURCE_TYPE value="WorkflowDSResourceType"/>
        <RESOURCE_KIND value="WorkflowDSResources"/>
        <RESOURCE_URI value=""/>
        <DATE_OF_CREATION value=""/>
    </HEADER>
    <BODY>
        <WORKFLOW_NAME>$name$</WORKFLOW_NAME>
        <WORKFLOW_DESCRIPTION>$desc$</WORKFLOW_DESCRIPTION>
        <WORKFLOW_INFO />
        <WORKFLOW_FAMILY>aggregator</WORKFLOW_FAMILY>
        <WORKFLOW_PRIORITY>$priority$</WORKFLOW_PRIORITY>
        <DATASOURCE id="$dsId$" interface="$interface$" />

        <CONFIGURATION status="WAIT_SYS_SETTINGS" start="MANUAL">
            <PARAMETERS>
                <PARAM name="harvestingMode"        description="Incremental or refresh mode"       required="true" managedBy="user"    type="string"   function="validValues(['REFRESH','INCREMENTAL'])">INCREMENTAL</PARAM>
                <PARAM name="collMdstoreId"         description="Store for collected records"       required="true" managedBy="system"  category="MDSTORE_ID"/>

				<PARAM name="passFullRecord"        description="True to pass the full record to x3m" managedBy="user"    type="boolean">false</PARAM>
                <PARAM name="transformationMode"    description="Incremental or refresh mode"       managedBy="user"    type="string"   function="validValues(['REFRESH','INCREMENTAL'])">REFRESH</PARAM>
                <PARAM name="verboseTransformationLogging"  description="Enable verbose logging of X3M"     required="false"    managedBy="user"    type="boolean"/>
				<PARAM name="mappingPolicyProfile"        description="Mapping policy to apply by X3M"    required="false"    managedBy="user"    category="TRANSFORMATION_RULE_ID"   type="string"   function="listProfiles('TransformationRuleDSResourceType', '//TITLE')"/>

				<PARAM name="cleaningRuleId"                description="Cleaning rule"                     required="true"     managedBy="user"    category="CLEANER_RULE_ID"          type="string"   function="listProfiles('CleanerDSResourceType', '//CLEANER_NAME')"/>
				<PARAM name="cleanMdstoreId"                description="Store for cleaned records"         required="true"     managedBy="system"  category="MDSTORE_ID"     />

				<PARAM name="indexId"               description="Identifier of the Index"           required="true" managedBy="system"  category="INDEX_ID"/>
            	<PARAM name="indexInterpretation"   description="Index Interpretation"              required="true" managedBy="system"  type="string">transformed</PARAM>
            	<PARAM name="feedingType"           description="Index feeding type"                required="true" managedBy="user"    type="string"   function="validValues(['REFRESH','INCREMENTAL'])">REFRESH</PARAM>
				<PARAM name="mappingUrl"           description="mapping url"                required="true" managedBy="user"    type="string"   ></PARAM>
				<PARAM name="publisherEndpoint"     description="AriadnePlus Publisher Endpoint"      required="true" managedBy="user"    type="string">http://localhost:8080/ariadneplus/publish</PARAM>
            </PARAMETERS>
            <WORKFLOW>
                <NODE isStart="true" name="collection" type="LaunchWorkflowTemplate">
                    <DESCRIPTION>Collect metadata</DESCRIPTION>
                    <PARAMETERS>
                    	<PARAM name="wfTemplateId" value="8536236a-7074-4155-9279-8cb2fcc8887a_V29ya2Zsb3dUZW1wbGF0ZURTUmVzb3VyY2VzL1dvcmtmbG93VGVtcGxhdGVEU1Jlc291cmNlVHlwZQ==" />
                    	<PARAM name="wfTemplateParams">
                    		<MAP>
                    		    <ENTRY key="format"         value="$format$" />
                    			<ENTRY key="dsId"           value="$dsId$" />
                    			<ENTRY key="dsName"         value="$dsName$" />
 								<ENTRY key="interface"      value="$interface$" />
	 							<ENTRY key="collMdstoreId"  ref="collMdstoreId" />
	 							<ENTRY key="harvestingMode" ref="harvestingMode" />
                    		</MAP>
                    	</PARAM>
                    </PARAMETERS>
                    <ARCS>
                        <ARC to="transform"/>
                    </ARCS>
                </NODE>
                <NODE name="transform" type="LaunchWorkflowTemplate">
                    <DESCRIPTION>Transform records</DESCRIPTION>
                    <PARAMETERS>
                        <PARAM name="wfTemplateId" value="bb36d5af-442c-488b-ad42-d9b068f6f1c0_V29ya2Zsb3dUZW1wbGF0ZURTUmVzb3VyY2VzL1dvcmtmbG93VGVtcGxhdGVEU1Jlc291cmNlVHlwZQ==" />
                        <PARAM name="wfTemplateParams">
                            <MAP>
                                <ENTRY key="dsId"                           value="$dsId$" />
                                <ENTRY key="interface"                      value="$interface$" />
                                <ENTRY key="collMdstoreId"                  ref="collMdstoreId" />
                                <ENTRY key="cleanMdstoreId"                 ref="cleanMdstoreId" />
                                <ENTRY key="enableSchemaValidation"         ref="enableSchemaValidation"/>
                                <ENTRY key="useDeclaredSchema"              ref="useDeclaredSchema"/>
                                <ENTRY key="schemaURL"                      ref="schemaURL"/>
                                <ENTRY key="mappingPolicyProfile"           ref="mappingPolicyProfile"/>
                                <ENTRY key="verboseTransformationLogging"   ref="verboseTransformationLogging"/>
                                <ENTRY key="cleaningRuleId"                 ref="cleaningRuleId"/>
                                <ENTRY key="transformationMode"             ref="transformationMode" />
                                <ENTRY key="passFullRecord"                 ref="passFullRecord" />
                                <ENTRY key="mappingUrl"                 ref="mappingUrl" />
                            </MAP>
                        </PARAM>
                    </PARAMETERS>
                    <ARCS>
                        <ARC to="index"/>
                    </ARCS>
                </NODE>
                <NODE name="index" type="LaunchWorkflowTemplate">
                    <DESCRIPTION>Index record</DESCRIPTION>
                    <PARAMETERS>
                        <PARAM name="wfTemplateId" value="23e81407-da05-46a6-a11e-928f92432922_V29ya2Zsb3dUZW1wbGF0ZURTUmVzb3VyY2VzL1dvcmtmbG93VGVtcGxhdGVEU1Jlc291cmNlVHlwZQ==" />
                        <PARAM name="wfTemplateParams">
                            <MAP>
                                <ENTRY key="dsId"               value="$dsId$" />
                                <ENTRY key="interface"          value="$interface$" />
                                <ENTRY key="cleanMdstoreId"     ref="cleanMdstoreId" />
                                <ENTRY key="indexId"            ref="indexId" />
                                <ENTRY key="feedingType"        ref="feedingType" />
                                <ENTRY key="interpretation"     ref="indexInterpretation" />
                            </MAP>
                        </PARAM>
                    </PARAMETERS>
                    <ARCS>
                        <ARC to="publishToGraphDB"/>
                    </ARCS>
                </NODE>
                <NODE name="publishToGraphDB" type="LaunchWorkflowTemplate">
                    <DESCRIPTION>Publish records to GraphDB</DESCRIPTION>
                    <PARAMETERS>
                        <PARAM name="wfTemplateId" value="7426eaaf-93c9-4914-b69a-c9d5c478405a_V29ya2Zsb3dUZW1wbGF0ZURTUmVzb3VyY2VzL1dvcmtmbG93VGVtcGxhdGVEU1Jlc291cmNlVHlwZQ=="/>
                        <PARAM name="wfTemplateParams">
                            <MAP>
                                <ENTRY key="dsId"               value="$dsId$" />
                                <ENTRY key="dsName"             value="$dsName$" />
                                <ENTRY key="interface"          value="$interface$" />
                                <ENTRY key="cleanMdstoreId"     ref="cleanMdstoreId"/>
                                <ENTRY key="publisherEndpoint"  ref="publisherEndpoint" />
                            </MAP>
                        </PARAM>
                    </PARAMETERS>
                    <ARCS>
                        <ARC to="success"/>
                    </ARCS>
                </NODE>
            </WORKFLOW>
            <DESTROY_WORKFLOW_TEMPLATE id="b54ff264-ed25-46a5-86df-dfbefb0b22be_V29ya2Zsb3dUZW1wbGF0ZURTUmVzb3VyY2VzL1dvcmtmbG93VGVtcGxhdGVEU1Jlc291cmNlVHlwZQ==">
            	<PARAMETERS>
	            	<PARAM name="dsId"                  value="$dsId$" />
	            	<PARAM name="dsName"                value="$dsName$" />
					<PARAM name="interface"             value="$interface$" />
					<PARAM name="collMdstoreId"         ref="collMdstoreId" />
					<PARAM name="cleanMdstoreId"        ref="cleanMdstoreId" />
					<PARAM name="indexId"               ref="indexId" />
				</PARAMETERS>
            </DESTROY_WORKFLOW_TEMPLATE>
        </CONFIGURATION>

        <NOTIFICATIONS/>
        
        <SCHEDULING enabled="false">
            <CRON>9 9 9 ? * *</CRON>
            <MININTERVAL>10080</MININTERVAL>
        </SCHEDULING>
        <STATUS/>
    </BODY>
</RESOURCE_PROFILE>
