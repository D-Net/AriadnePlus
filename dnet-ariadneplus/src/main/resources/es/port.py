#copies indexes
from elasticsearch import Elasticsearch
from elasticsearch_dsl import *

es_ariadnetest = Elasticsearch(hosts = [{'host':'elastic-test.ariadne.d4science.org', 'port':9200}])
index_name = "aat_ariadne"
s = Search(using=es_ariadnetest, index=index_name).query(Q('match_all'))

es_ariadne = Elasticsearch(hosts = [{'host':'elastic.ariadne.d4science.org', 'port':9200}])

for entry in s.scan():
    es_ariadne.index(index=index_name, id=entry.__dict__['meta']['id'], body=entry.__dict__['_d_'])