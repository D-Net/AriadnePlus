package eu.dnetlib.data.collector.plugins.ariadneplus;

import com.ximpleware.*;
import eu.dnetlib.data.collector.ThreadSafeIterator;
import eu.dnetlib.rmi.data.CollectorServiceRuntimeException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;

public class XMLsFolderIterator extends ThreadSafeIterator {

    private static final Log log = LogFactory.getLog(XMLsFolderIterator.class);

    private Iterator<String> iterator;
    private String namespaceList;

    public XMLsFolderIterator(final Iterator<String> recordIterator, final String namespaceList){

        this.iterator = recordIterator;
        this.namespaceList = namespaceList;
    }

    @Override
    public boolean doHasNext() {
        return iterator.hasNext();
    }

    @Override
    public String doNext()  {
        String record = iterator.next();
        try {
            return addCustomNamespace(record, getNamespaceList());
        } catch (Exception e) {
            log.warn("Skipping record because of exception "+e);
            log.debug("Skipped record: "+record);
            if(this.hasNext()){
                return this.next();
            }
            else return "";
        }
    }

    protected String addCustomNamespace(final String xml, String namespaceList) {

        try {
            VTDGen vg = new VTDGen();
            vg.setDoc(xml.getBytes());
            vg.parse(false); // namespace unaware to all name space nodes addressable using xpath @*
            VTDNav vn = vg.getNav();
            XMLModifier xm = new XMLModifier(vn);

            namespaceList = " ".concat(namespaceList).concat(" ");
            byte[] attrBytes = namespaceList.getBytes();

            vn.toElement(VTDNav.ROOT);
            xm.insertAttribute(attrBytes);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            xm.output(baos);
            return baos.toString();
        } catch(ParseException | ModifyException | NavException | IOException | TranscodeException e){
            log.error("Cannot add namespace declarations to element: "+xml);
            throw new CollectorServiceRuntimeException("Cannot add namespace declarations to element", e);
        }
    }

    public Iterator<String> getIterator() {
        return iterator;
    }

    public void setIterator(final Iterator<String> iterator) {
        this.iterator = iterator;
    }

    public String getNamespaceList() {
        return namespaceList;
    }

    public void setNamespaceList(String namespaceList) {
        this.namespaceList = namespaceList;
    }
}
