package eu.dnetlib.data.collector.plugins.ariadneplus.thanados;

import eu.dnetlib.data.collector.plugins.httplist.HttpListIterator;
import eu.dnetlib.rmi.data.CollectorServiceException;
import eu.dnetlib.rmi.data.InterfaceDescriptor;
import eu.dnetlib.rmi.data.plugin.AbstractCollectorPlugin;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Date;

public class ThanadosCollectorPlugin extends AbstractCollectorPlugin {

	@Override
	public Iterable<String> collect(final InterfaceDescriptor interfaceDescriptor, final String fromDate, final String untilDate) {
		final String baseUrl = interfaceDescriptor.getBaseUrl();
		final String listAddress = interfaceDescriptor.getParams().get("listUrl");

		return () -> new ThanadosIterator(baseUrl, listAddress, fromDate);
	}
}
