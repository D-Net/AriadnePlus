package eu.dnetlib.data.collector.plugins.ariadneplus.ads;

import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * The Class FilesystemIterable.
 *
 * 
 */
public class ADSIterable implements Iterable<String> {

	/** The Constant log. */
	private static final Log log = LogFactory.getLog(ADSIterable.class);

	private Iterator<String> recordIterator;
	
	public ADSIterable(Iterator<String> recordIterator ) {
		this.recordIterator = recordIterator;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<String> iterator() {
		return recordIterator;
	}

	public Iterator<String> getRecordIterator() {
		return recordIterator;
	}

	public void setRecordIterator(Iterator<String> recordIterator) {
		this.recordIterator = recordIterator;
	}
}