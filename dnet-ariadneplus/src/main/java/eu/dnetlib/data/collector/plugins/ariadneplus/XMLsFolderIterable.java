package eu.dnetlib.data.collector.plugins.ariadneplus;

import java.util.Iterator;

public class XMLsFolderIterable implements Iterable<String>{

    private Iterator<String> recordIterator;

    public XMLsFolderIterable(Iterator<String> recordIterator ) {
        this.recordIterator = recordIterator;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Iterable#iterator()
     */
    @Override
    public Iterator<String> iterator() {
        return recordIterator;
    }

    public Iterator<String> getRecordIterator() {
        return recordIterator;
    }

    public void setRecordIterator(Iterator<String> recordIterator) {
        this.recordIterator = recordIterator;
    }
}
