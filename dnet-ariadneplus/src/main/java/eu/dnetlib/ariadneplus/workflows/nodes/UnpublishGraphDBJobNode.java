package eu.dnetlib.ariadneplus.workflows.nodes;

import java.net.URI;

import eu.dnetlib.msro.workflows.graph.Arc;
import eu.dnetlib.msro.workflows.nodes.AsyncJobNode;
import eu.dnetlib.msro.workflows.procs.Env;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class UnpublishGraphDBJobNode extends AsyncJobNode {

	private static final Log log = LogFactory.getLog(UnpublishGraphDBJobNode.class);

	private String datasourceInterface;
	private String publisherEndpoint;

	@Override
	protected String execute(final Env env) throws Exception {
		URI getURI = new URIBuilder(getPublisherEndpoint()).addParameter("datasourceApi",  getDatasourceInterface()).build();
		HttpClient c = HttpClients.createDefault();
		HttpResponse res = c.execute(new HttpGet(getURI));
		String nTriples = EntityUtils.toString(res.getEntity());

		log.info("Deleted " + nTriples + " triples in GraphDB server");

		env.setAttribute("triples", nTriples);
		return Arc.DEFAULT_ARC;

	}

	public String getDatasourceInterface() {
		return datasourceInterface;
	}

	public void setDatasourceInterface(final String datasourceInterface) {
		this.datasourceInterface = datasourceInterface;
	}

	public String getPublisherEndpoint() {
		return publisherEndpoint;
	}

	public void setPublisherEndpoint(final String publisherEndpoint) {
		this.publisherEndpoint = publisherEndpoint;
	}
}
