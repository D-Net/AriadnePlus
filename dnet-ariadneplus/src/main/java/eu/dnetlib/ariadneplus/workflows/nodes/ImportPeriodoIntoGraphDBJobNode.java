package eu.dnetlib.ariadneplus.workflows.nodes;

import com.google.common.collect.Lists;
import eu.dnetlib.msro.workflows.graph.Arc;
import eu.dnetlib.msro.workflows.nodes.AsyncJobNode;
import eu.dnetlib.msro.workflows.procs.Env;
import eu.dnetlib.msro.workflows.util.WorkflowsConstants;
import eu.dnetlib.rmi.manager.MSROException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.net.ConnectException;
import java.util.List;


public class ImportPeriodoIntoGraphDBJobNode extends AsyncJobNode {

	private static final Log log = LogFactory.getLog(ImportPeriodoIntoGraphDBJobNode.class);

	private String dataUrl;
	private String context;
	private String publisherEndpoint;

	//for parallel requests to the publisher endpoint
	private int nThreads = 5;

	@Override
	protected String execute(final Env env) throws Exception {

		int statusCode = -1;
		String loadedResult = "noResult";
		log.info("Publisher endpoint: " + getPublisherEndpoint());
		log.info("Context: " + getContext());
		log.info("Data url: " + getDataUrl());
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		cm.setMaxTotal(nThreads);
		CloseableHttpClient client = HttpClients.custom().setConnectionManager(cm).build();
		
		log.info("Feed from url endpoint: " + getFeedFromUrlEndpoint());
		CloseableHttpResponse responsePOST = null;
		try {
			HttpPost post = new HttpPost(getFeedFromUrlEndpoint());
			List<NameValuePair> params = Lists.newArrayList();
			params.add(new BasicNameValuePair("dataUrl", getDataUrl()));
			params.add(new BasicNameValuePair("context", getContext()));
			UrlEncodedFormEntity entity =  new UrlEncodedFormEntity(params, "UTF-8");
			post.setEntity(entity);
			responsePOST = client.execute(post);
			statusCode = responsePOST.getStatusLine().getStatusCode();
			switch (statusCode) {
			case 200:
				log.info("data loaded completed");
				loadedResult = "data loaded from url "+ getDataUrl() + " into context "+ getContext();
				break;
			default:
				log.error("error loading data into graphDB  " + responsePOST.getStatusLine().getStatusCode() + ": " + responsePOST.getStatusLine().getReasonPhrase());
				break;
			}
		} catch (ConnectException ce) {
			log.error(ce);
			throw new MSROException("Unable to connect to Publisher endpoint" + getFeedFromUrlEndpoint());
		}
		catch (IOException e) {
			log.error(e);
			throw new MSROException("IO error " + getFeedFromUrlEndpoint());
		}
		finally{
			if(responsePOST != null) responsePOST.close();
			client.close();
			cm.shutdown();
		}

		env.setAttribute(WorkflowsConstants.MAIN_LOG_PREFIX + "statusCode", Integer.toString(statusCode));
		env.setAttribute(WorkflowsConstants.MAIN_LOG_PREFIX + "loadedResult", loadedResult);

		log.info(loadedResult);
		if (statusCode!=200) {
			throw new Exception("Error from Publisher endpoint [ status code: " + statusCode + " ]");
		}
		return Arc.DEFAULT_ARC;
	}

	public String getPublisherEndpoint() {
		return publisherEndpoint;
	}
	
	private String getFeedFromUrlEndpoint() {
		return publisherEndpoint.concat("/feedFromURL");
	}

	public void setPublisherEndpoint(final String publisherEndpoint) {
		this.publisherEndpoint = publisherEndpoint;
	}

	public String getDataUrl() {
		return dataUrl;
	}

	public void setDataUrl(String dataUrl) {
		this.dataUrl = dataUrl;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}
}
