package eu.dnetlib.ariadneplus.publisher;

import eu.dnetlib.ariadneplus.publisher.AriadnePlusPublisherHelper.AriadnePlusTargets;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * @author enrico.ottonello
 *
 */

@RestController
public class AriadnePlusPublisherController {

	private static final Log log = LogFactory.getLog(AriadnePlusPublisherController.class);
	
	private static final String DEFAULT_TARGET_ENDPOINT = "GRAPHDB";
	
	@Autowired
	private AriadnePlusPublisherHelper ariadneplusPublisherHelper;

	@RequestMapping(value = "/version", method = RequestMethod.GET)
	public String version() throws AriadnePlusPublisherException {
		return "4.0.0-SNAPSHOT";
	}
	
	@RequestMapping(value = "/publish", method = RequestMethod.POST)
	public void publish(@RequestBody final String record) throws AriadnePlusPublisherException, IOException {
		getAriadnePlusPublisherHelper().publish(record, getTarget(DEFAULT_TARGET_ENDPOINT));
	}

	@RequestMapping(value = "/feedProvenance", method = RequestMethod.POST)
	public void feedProvenance(@RequestParam final String datasource, @RequestParam final String datasourceApi) throws AriadnePlusPublisherException {
		getAriadnePlusPublisherHelper().feedProvenance(datasource, datasourceApi, getTarget(DEFAULT_TARGET_ENDPOINT));
	}
	
	@RequestMapping(value = "/dropDatasourceApiGraph", method = RequestMethod.POST)
	public void dropDatasourceApisPartitionInfo(@RequestParam final String datasourceApi) throws AriadnePlusPublisherException {
		getAriadnePlusPublisherHelper().dropDatasourceApiGraph(datasourceApi, getTarget(DEFAULT_TARGET_ENDPOINT));
	}

	@RequestMapping(value = "/unpublish", method = RequestMethod.GET)
	public void unpublish(@RequestParam final String datasourceApi) throws AriadnePlusPublisherException {
		getAriadnePlusPublisherHelper().unpublish(datasourceApi, getTarget(DEFAULT_TARGET_ENDPOINT));
	}

	private AriadnePlusTargets getTarget(String value) {
		return AriadnePlusTargets.valueOf(value);
	}

	public AriadnePlusPublisherHelper getAriadnePlusPublisherHelper() {
		return ariadneplusPublisherHelper;
	}

	public void setAriadnePlusPublisherHelper(final AriadnePlusPublisherHelper ariadneplusPublisherHelper) {
		this.ariadneplusPublisherHelper = ariadneplusPublisherHelper;
	}

	@RequestMapping(value = "/updateSparql", method = RequestMethod.POST)
	public String updateSparql(@RequestBody final String queryValue) throws AriadnePlusPublisherException {
		return getAriadnePlusPublisherHelper().updateSparql(queryValue, getTarget(DEFAULT_TARGET_ENDPOINT));
	}

	@RequestMapping(value = "/feedFromURL", method = RequestMethod.POST)
	public String feedFromURL(@RequestParam final String dataUrl, @RequestParam final String context) throws AriadnePlusPublisherException {
		return getAriadnePlusPublisherHelper().feedFromURL(dataUrl, context, getTarget(DEFAULT_TARGET_ENDPOINT));
	}

	@RequestMapping(value = "/indexOnES", method = RequestMethod.GET)
	public String indexOnES(@RequestParam final String datasource, @RequestParam final String collectionId) throws AriadnePlusPublisherException {
		return getAriadnePlusPublisherHelper().indexOnES(datasource, collectionId, getTarget(DEFAULT_TARGET_ENDPOINT));
	}

	@RequestMapping(value = "/selectIdentifiers", method = RequestMethod.GET)
	public List<String> selectIdentifiers(@RequestParam final String datasource, @RequestParam final String collectionId,
										  @RequestParam String resourceType) throws AriadnePlusPublisherException {
		return getAriadnePlusPublisherHelper().selectIdentifiers(datasource, collectionId,
				resourceType, getTarget(DEFAULT_TARGET_ENDPOINT));
	}

	@RequestMapping(value = "/indexOnESByIdentifier", method = RequestMethod.POST)
	public String indexOnESByIdentifier(@RequestParam final String datasource, @RequestParam final String collectionId,
										 @RequestParam String resourceType, @RequestParam String identifier) throws AriadnePlusPublisherException {
		return getAriadnePlusPublisherHelper().indexOnESByIdentifier(datasource, collectionId,
				resourceType, identifier, getTarget(DEFAULT_TARGET_ENDPOINT));
	}
}