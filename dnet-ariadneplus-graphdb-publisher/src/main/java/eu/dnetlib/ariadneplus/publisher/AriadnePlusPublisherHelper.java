package eu.dnetlib.ariadneplus.publisher;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.dnetlib.ariadneplus.graphdb.GraphDBClient;
import eu.dnetlib.ariadneplus.graphdb.GraphDBClientFactory;

import java.util.List;

/**
 * @author enrico.ottonello
 *
 */

@Component
public class AriadnePlusPublisherHelper {

	private static final Log log = LogFactory.getLog(AriadnePlusPublisherHelper.class);

	public enum AriadnePlusTargets{
		GRAPHDB
	}

	@Autowired
	private GraphDBClientFactory graphdbClientFactory;

	public void publish(final String record, final AriadnePlusTargets target) throws AriadnePlusPublisherException {
		switch(target){
		case GRAPHDB:
			publishGraphDB(record);
			break;
		default: throw new AriadnePlusPublisherException("Target "+target+" not supported yet");
		}

	}

	public void feedProvenance(final String datasource, final String datasourceApi, final AriadnePlusTargets target) throws AriadnePlusPublisherException {
		switch(target){
		case GRAPHDB:
			feedProvenance(datasource, datasourceApi);
			break;
		default: throw new AriadnePlusPublisherException("Target "+target+" not supported yet");
		}

	}
	
	public void dropDatasourceApiGraph(final String datasourceApi, final AriadnePlusTargets target) throws AriadnePlusPublisherException {
		switch(target){
		case GRAPHDB:
			dropDatasourceApiGraph(datasourceApi);
			break;
		default: throw new AriadnePlusPublisherException("Target "+target+" not supported yet");
		}

	}
	
	public long unpublish(final String datasourceInterface, final AriadnePlusTargets target) throws AriadnePlusPublisherException {
		long res = 0;
		switch(target){
		case GRAPHDB:
			res = unpublishGraphDB(datasourceInterface);
			break;
		default: throw new AriadnePlusPublisherException("Target "+target+" not supported yet");
		}
		return res;
	}

	public String updateSparql(final String queryValue, final AriadnePlusTargets target) throws AriadnePlusPublisherException {
		String res;
		switch(target){
			case GRAPHDB:
				res = updateSparqlGraphDB(queryValue);
				break;
			default: throw new AriadnePlusPublisherException("Target "+target+" not supported yet");
		}
		return res;
	}

	public String feedFromURL(final String dataUrl, final String context, final AriadnePlusTargets target) throws AriadnePlusPublisherException {
		String res;
		switch(target){
			case GRAPHDB:
				res = feedFromURL(dataUrl, context);
				break;
			default: throw new AriadnePlusPublisherException("Target "+target+" not supported yet");
		}
		return res;
	}

	public String indexOnES(final String datasource, final String collectionId, final AriadnePlusTargets target) throws AriadnePlusPublisherException {
		String res;
		switch(target){
			case GRAPHDB:
				res = indexOnES(datasource, collectionId);
				break;
			default: throw new AriadnePlusPublisherException("Target "+target+" not supported yet");
		}
		return res;
	}

	private void publishGraphDB(final String record) throws AriadnePlusPublisherException {
		GraphDBClient graphDBClient = this.graphdbClientFactory.getGraphDBClient();
		graphDBClient.callFeedService(record);
	}
	
	private void feedProvenance(final String datasource, final String datasourceApi) throws AriadnePlusPublisherException {
		log.debug("Feed Provenance " + datasource + " - " + datasourceApi);
		GraphDBClient graphDBClient = this.graphdbClientFactory.getGraphDBClient();
		graphDBClient.feedProvenance(datasource, datasourceApi);
	}

	private void dropDatasourceApiGraph(final String datasourceApi) throws AriadnePlusPublisherException {
		log.debug("Drop DatasourceApis Partition Info " + datasourceApi);
		GraphDBClient graphDBClient = this.graphdbClientFactory.getGraphDBClient();
		graphDBClient.dropDatasourceApiGraph(datasourceApi);
	}
	
	private long unpublishGraphDB(final String datasourceInterface) {
		log.info("Unpublishing from graphdb "+datasourceInterface);
		GraphDBClient graphDBClient = this.graphdbClientFactory.getGraphDBClient();
		
		return 0;
	}

	private String updateSparqlGraphDB(final String queryValue) throws AriadnePlusPublisherException {
		log.info("updateSparqlGraphDB "+queryValue);
		GraphDBClient graphDBClient = this.graphdbClientFactory.getGraphDBClient();
		return graphDBClient.updateSparql(queryValue);
	}

	private String feedFromURL(final String dataUrl, final String context) throws AriadnePlusPublisherException {
		log.info("feedFromURL "+dataUrl + " " + context);
		GraphDBClient graphDBClient = this.graphdbClientFactory.getGraphDBClient();
		return graphDBClient.feedFromURL(dataUrl, context);
	}

	private String indexOnES(final String datasource, final String collectionId) throws AriadnePlusPublisherException {
		GraphDBClient graphDBClient = this.graphdbClientFactory.getGraphDBClient();
		return graphDBClient.indexOnES(datasource, collectionId);
	}

	public List<String> selectIdentifiers(String datasource, String collectionId, String resourceType, AriadnePlusTargets target) throws AriadnePlusPublisherException {
		GraphDBClient graphDBClient = this.graphdbClientFactory.getGraphDBClient();
		return graphDBClient.selectIdentifiers(datasource, collectionId, resourceType);
	}

	public String indexOnESByIdentifier(String datasource, String collectionId, String resourceType, String identifier, AriadnePlusTargets target) throws AriadnePlusPublisherException {
		GraphDBClient graphDBClient = this.graphdbClientFactory.getGraphDBClient();
		return graphDBClient.indexOnESByIdentifier(datasource, collectionId, resourceType, identifier);
	}
}
