package eu.dnetlib.ariadneplus.publisher;

/**
 * @author enrico.ottonello
 *
 */

public class AriadnePlusPublisherException extends Exception{

	public AriadnePlusPublisherException() {
	}

	public AriadnePlusPublisherException(final String message) {
		super(message);
	}

	public AriadnePlusPublisherException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public AriadnePlusPublisherException(final Throwable cause) {
		super(cause);
	}

	public AriadnePlusPublisherException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
