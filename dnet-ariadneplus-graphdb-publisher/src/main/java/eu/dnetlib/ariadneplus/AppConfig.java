package eu.dnetlib.ariadneplus;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.TemplateExceptionHandler;

/**
 * @author enrico.ottonello
 *
 */

@Configuration
public class AppConfig {

	@Bean
	public RestTemplate jrrRestTemplate(){
		//TODO: move configuration here from CatalogueRegistrator?
		return new RestTemplateBuilder().build();
	}


	@Bean
	public freemarker.template.Configuration freemarkerConfig(){
		freemarker.template.Configuration config = new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_27);
		ClassTemplateLoader ctl = new ClassTemplateLoader(getClass(), "/eu/dnetlib/ariadneplus/sparql");
		config.setTemplateLoader(ctl);
		config.setDefaultEncoding("UTF-8");
		// Sets how errors will appear.
		// During web page *development* TemplateExceptionHandler.HTML_DEBUG_HANDLER is better.
		config.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

		// Don't log exceptions inside FreeMarker that it will thrown at you anyway:
		config.setLogTemplateExceptions(false);

		// Wrap unchecked exceptions thrown during template processing into TemplateException-s.
		config.setWrapUncheckedExceptions(true);

		return config;
	}
}