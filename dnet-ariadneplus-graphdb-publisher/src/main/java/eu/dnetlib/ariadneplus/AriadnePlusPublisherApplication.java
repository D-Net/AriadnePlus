package eu.dnetlib.ariadneplus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author enrico.ottonello
 *
 */

@SpringBootApplication
public class AriadnePlusPublisherApplication {

	public static void main(String[] args) {
		SpringApplication.run(AriadnePlusPublisherApplication.class, args);
	}

}
