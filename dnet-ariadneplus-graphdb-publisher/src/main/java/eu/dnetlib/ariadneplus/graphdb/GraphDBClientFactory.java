package eu.dnetlib.ariadneplus.graphdb;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.dnetlib.ariadneplus.rdf.RecordParserHelper;

/**
 * @author enrico.ottonello
 *
 */

@Component
public class GraphDBClientFactory {

	private static final Log log = LogFactory.getLog(GraphDBClientFactory.class);
	@Value("${graphdb.serverUrl}")
	private String graphDBServerUrl;
	@Value("${graphdb.baseURI}")
	private String graphDBBaseURI;
	@Value("${graphdb.writer.user}")
	private String writerUser;
	@Value("${graphdb.writer.pwd}")
	private String writerPwd;
	@Value("${graphdb.repository}")
	private String repository;
	
	@Autowired
	private RecordParserHelper recordParserHelper;

	@Autowired
	private GraphDBClient graphDBClient;

	public GraphDBClient getGraphDBClient() {
		graphDBClient.setup(recordParserHelper, graphDBServerUrl, graphDBBaseURI, writerUser, writerPwd, repository);
		return graphDBClient;
	}

	public RecordParserHelper getRecordParserHelper() {
		return recordParserHelper;
	}

	public void setRecordParserHelper(final RecordParserHelper recordParserHelper) {
		this.recordParserHelper = recordParserHelper;
	}

	
	public String getGraphDBServerUrl() {
		return graphDBServerUrl;
	}

	
	public void setGraphDBServerUrl(String graphDBServerUrl) {
		this.graphDBServerUrl = graphDBServerUrl;
	}

	
	public String getGraphDBBaseURI() {
		return graphDBBaseURI;
	}

	
	public void setGraphDBBaseURI(String graphDBBaseURI) {
		this.graphDBBaseURI = graphDBBaseURI;
	}

	
	public String getWriterUser() {
		return writerUser;
	}

	
	public void setWriterUser(String writerUser) {
		this.writerUser = writerUser;
	}

	
	public String getWriterPwd() {
		return writerPwd;
	}

	
	public void setWriterPwd(String writerPwd) {
		this.writerPwd = writerPwd;
	}
}
