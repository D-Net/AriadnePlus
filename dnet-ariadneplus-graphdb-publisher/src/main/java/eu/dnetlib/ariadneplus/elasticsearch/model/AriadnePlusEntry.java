package eu.dnetlib.ariadneplus.elasticsearch.model;

import com.google.gson.Gson;
import eu.dnetlib.ariadneplus.reader.utils.ESUtils;
import org.apache.commons.compress.utils.Lists;

import java.util.List;

public class AriadnePlusEntry {
    private List<DerivedSubject> derivedSubject;
    private String  accessPolicy;
    private String accessRights;
    private List<AriadneSubject> ariadneSubject;
    private List<AgentInfo> contributor;
    private List<AgentInfo> creator;
    private List<TextLang> description;
    private String extent;
    private String identifier;
    private List<String> isPartOf;
    private String issued;
    private String landingPage;
    private String language;
    private String modified;
    private List<NativeSubject> nativeSubject;
    private String originalId;
    private List<AgentInfo> owner;
    private List<PublisherInfo> publisher;
    private List<AriadneResource> is_about;
    private String resourceType;
    private AriadneResource has_type;
    private List<AgentInfo> responsible;
    private List<Spatial> spatial;
    private List<Temporal> temporal;
    private List<TextLang> title;
    private List<NativePeriod> nativePeriod;
    private String wasCreated;
    private List<DigitalImage> digitalImage;
    private List<String> otherId;
    private List<CountryInfo> country;
    private List<AriadneResource> dataType;

    private transient String uniqueIsPartOf;
    private transient String typeURI;
    private transient String typeLabel;

    public List<AgentInfo> getContributor() {
        return contributor;
    }

    public void setContributor(List<AgentInfo> contributor) {
        this.contributor = contributor;
    }

    public String getAccessPolicy() {
        return accessPolicy;
    }

    public void setAccessPolicy(String accessPolicy) {
        this.accessPolicy = accessPolicy;
    }

    public String getAccessRights() {
        return accessRights;
    }

    public void setAccessRights(String accessRights) {
        this.accessRights = accessRights;
    }

    public List<AgentInfo> getCreator() {
        return creator;
    }

    public void setCreator(List<AgentInfo> creator) {
        this.creator = creator;
    }

    public List<DerivedSubject> getDerivedSubject() {
        return derivedSubject;
    }

    public void setDerivedSubject(List<DerivedSubject> derivedSubject) {
        this.derivedSubject = derivedSubject;
    }

    public String getExtent() {
        return extent;
    }

    public void setExtent(String extent) {
        this.extent = extent;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public List<String> getIsPartOf() {
        return isPartOf;
    }

    public void setIsPartOf(List<String> isPartOf) {
        this.isPartOf = isPartOf;
    }

    public void setIsPartOf(String isPartOf) {
        if (this.isPartOf==null) {
            this.isPartOf = Lists.newArrayList();
        }
        this.isPartOf.add(isPartOf);
    }

    public String getIssued() {
        return issued;
    }

    public void setIssued(String issued) {
        this.issued = ESUtils.getESFormatDate(issued);
    }

    public String getLandingPage() {
        return landingPage;
    }

    public void setLandingPage(String landingPage) {
        this.landingPage = landingPage;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        if (language!=null && language.equals("eng")) {
            this.language = "en";
        }
        else {
            this.language = language;
        }
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = ESUtils.getESFormatDate(modified);
    }

    public List<NativeSubject> getNativeSubject() {
        return nativeSubject;
    }

    public void setNativeSubject(List<NativeSubject> nativeSubject) {
        this.nativeSubject = nativeSubject;
    }

    public String getOriginalId() {
        return originalId;
    }

    public void setOriginalId(String originalId) {
        this.originalId = originalId;
    }

    public List<AgentInfo> getOwner() {
        return owner;
    }

    public void setOwner(List<AgentInfo> owner) {
        this.owner = owner;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }


    public List<Spatial> getSpatial() {
        return spatial;
    }

    public void setSpatial(List<Spatial> spatial) {
        if (this.spatial==null) {
            this.spatial = spatial;
        }
        else {
            this.spatial.addAll(spatial);
        }
    }

    public List<Temporal> getTemporal() {
        return temporal;
    }

    public void setTemporal(List<Temporal> temporal) {
        this.temporal = temporal;
    }

    public void setHas_type(AriadneResource has_type) {
        this.has_type = has_type;
    }

    public void setResponsible(List<AgentInfo> responsible) {
        this.responsible = responsible;
    }

    public void setWasCreated(String wasCreated) {
        this.wasCreated = ESUtils.getESFormatDate(wasCreated);
    }

    public String getUniqueIsPartOf() {
        return uniqueIsPartOf;
    }

    public void setUniqueIsPartOf(String uniqueIsPartOf) {
        this.uniqueIsPartOf = uniqueIsPartOf;
    }

    public static AriadnePlusEntry fromJson(String json){
        return new Gson().fromJson(json, AriadnePlusEntry.class);
    }

    public AriadneResource getHas_type() {
        return has_type;
    }

    public List<AgentInfo> getResponsible() {
        return responsible;
    }

    public String getWasCreated() {
        return wasCreated;
    }

    public List<AriadneSubject> getAriadneSubject() {
        return ariadneSubject;
    }

    public void setAriadneSubject(List<AriadneSubject> ariadneSubject) {
        this.ariadneSubject = ariadneSubject;
    }

    public List<NativePeriod> getNativePeriod() {
        return nativePeriod;
    }

    public void setNativePeriod(List<NativePeriod> nativePeriod) {
        this.nativePeriod = nativePeriod;
    }

    public String getTypeURI() {
        return typeURI;
    }

    public void setTypeURI(String typeURI) {
        this.typeURI = typeURI;
    }

    public String getTypeLabel() {
        return typeLabel;
    }

    public void setTypeLabel(String typeLabel) {
        this.typeLabel = typeLabel;
    }

    public List<AriadneResource> getIs_about() {
        return is_about;
    }

    public void setIs_about(List<AriadneResource> is_about) {
        this.is_about = is_about;
    }

    public List<DigitalImage> getDigitalImage() {
        return digitalImage;
    }

    public void setDigitalImage(List<DigitalImage> digitalImage) {
        this.digitalImage = digitalImage;
    }

    public List<TextLang> getDescription() {
        return description;
    }

    public void setDescription(List<TextLang> description) {
        this.description = description;
    }

    public List<TextLang> getTitle() {
        return title;
    }

    public void setTitle(List<TextLang> title) {
        this.title = title;
    }

    public  String toJson(){
        return new Gson().toJson(this);
    }

    public List<PublisherInfo> getPublisher() {
        return publisher;
    }

    public void setPublisher(List<PublisherInfo> publisher) {
        this.publisher = publisher;
    }

    public List<String> getOtherId() {
        return otherId;
    }

    public void setOtherId(String otherId) {
        if (this.otherId==null) {
            this.otherId = Lists.newArrayList();
        }
       this.otherId.add(otherId);
    }

    public List<CountryInfo> getCountry() {
        return country;
    }

    public void setCountry(List<CountryInfo> country) {
        this.country = country;
    }

    public List<AriadneResource> getDataType() {
        return dataType;
    }

    public void setDataType(List<AriadneResource> dataType) {
        this.dataType = dataType;
    }
}