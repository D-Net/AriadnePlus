package eu.dnetlib.ariadneplus.elasticsearch.model;

import com.google.gson.Gson;

public class AgentInfo {
    protected String homepage = new String("");
    protected String institution = new String("");
    protected String name = new String("");
    protected String agentIdentifier = new String("");

    public AgentInfo() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getAgentIdentifier() {
        return agentIdentifier;
    }

    public void setAgentIdentifier(String agentIdentifier) {
        this.agentIdentifier = agentIdentifier;
    }

    public static AgentInfo fromJson(String json){
        return new Gson().fromJson(json, AgentInfo.class);
    }
}
