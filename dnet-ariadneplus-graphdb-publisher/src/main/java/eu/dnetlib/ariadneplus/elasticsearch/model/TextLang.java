package eu.dnetlib.ariadneplus.elasticsearch.model;

import com.google.gson.Gson;

public class TextLang {
    private String text;
    private String language;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public TextLang() {
    }

    public static TextLang fromJson(String json){
        return new Gson().fromJson(json, TextLang.class);
    }
}
