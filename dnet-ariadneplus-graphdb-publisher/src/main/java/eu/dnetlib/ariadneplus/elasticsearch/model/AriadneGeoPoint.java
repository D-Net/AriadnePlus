package eu.dnetlib.ariadneplus.elasticsearch.model;

import com.google.gson.Gson;

public class AriadneGeoPoint {
    private String lat;
    private String lon;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public AriadneGeoPoint() {

    }

    public static AriadneGeoPoint fromJson(String json){
        return new Gson().fromJson(json, AriadneGeoPoint.class);
    }
}
