package eu.dnetlib.ariadneplus.elasticsearch.model;

import com.google.gson.Gson;

public class DigitalImage {

    private String ariadneUri;
    private String primary;
    private String providerUri;

    public String getAriadneUri() {
        return ariadneUri;
    }

    public void setAriadneUri(String ariadneUri) {
        this.ariadneUri = ariadneUri;
    }

    public String getPrimary() {
        return primary;
    }

    public void setPrimary(String primary) {
        this.primary = primary;
    }

    public String getProviderUri() {
        return providerUri;
    }

    public void setProviderUri(String providerUri) {
        this.providerUri = providerUri;
    }

    public DigitalImage() {
    }

    public static DigitalImage fromJson(String json){
        return new Gson().fromJson(json, DigitalImage.class);
    }
}
