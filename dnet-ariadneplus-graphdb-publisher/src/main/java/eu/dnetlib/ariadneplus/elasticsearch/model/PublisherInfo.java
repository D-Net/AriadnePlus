package eu.dnetlib.ariadneplus.elasticsearch.model;

public class PublisherInfo extends AgentInfo {
    private String email = new String("");

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
