package eu.dnetlib.ariadneplus.elasticsearch.model;

import com.google.gson.Gson;

public class DerivedSubject {
    private String prefLabel;
    private String source;
    private String id;
    private String lang;

    public String getPrefLabel() {
        return prefLabel;
    }

    public void setPrefLabel(String prefLabel) {
        this.prefLabel = prefLabel;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public DerivedSubject() {
    }

    public static DerivedSubject fromJson(String json){
        return new Gson().fromJson(json, DerivedSubject.class);
    }
}
