package eu.dnetlib.ariadneplus.elasticsearch.model;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.Map;

public class NativeSubject {
    private String prefLabel;
    private String rdfAbout;

    public String getPrefLabel() {
        return prefLabel;
    }

    public void setPrefLabel(String prefLabel) {
        this.prefLabel = prefLabel;
    }

    public String getRdfAbout() {
        return rdfAbout;
    }

    public void setRdfAbout(String rdfAbout) {
        this.rdfAbout = rdfAbout;
    }

    public NativeSubject() {
    }

    public static NativeSubject fromJson(String json){
        return new Gson().fromJson(json, NativeSubject.class);
    }

    public static NativeSubject fromRDFJson(JsonElement json) {
        NativeSubject pi = new NativeSubject();
        for (Map.Entry<String, JsonElement> entry : json.getAsJsonObject().entrySet()){
            switch (entry.getKey()){
                case "https://www.ariadne-infrastructure.eu/property/prefLabel" :
                    pi.setPrefLabel(entry.getValue().getAsJsonArray().get(0).getAsJsonObject().get("value").getAsString());
                    break;
                case "https://www.ariadne-infrastructure.eu/property/rdfAbout":
                    pi.setRdfAbout(entry.getValue().getAsJsonArray().get(0).getAsJsonObject().get("value").getAsString());
                    break;

            }
        }

        return pi;
    }
}
