package eu.dnetlib.ariadneplus.elasticsearch.model;

import com.google.gson.Gson;

import java.util.HashMap;

public class AriadneSubject {
    private String prefLabel;

    public String getPrefLabel() {
        return prefLabel;
    }

    public void setPrefLabel(String prefLabel) {
        this.prefLabel = prefLabel;
    }

    private transient HashMap<String, Long> typeValues =new HashMap<>();

    public AriadneSubject() {
        typeValues.put("Site/monument", new Long(10));
        typeValues.put("Fieldwork", new Long(11));
        typeValues.put("Fieldwork report", new Long(12));
        typeValues.put("Scientific analysis", new Long(13));
        typeValues.put("Date", new Long(14));
        typeValues.put("Artefact", new Long(15));
        typeValues.put("Fieldwork archive", new Long(16));
        typeValues.put("Inscription", new Long(17));
        typeValues.put("Burial", new Long(18));
        typeValues.put("Rock Art", new Long(19));
    }

    public static AriadneSubject fromJson(String json){
        return new Gson().fromJson(json, AriadneSubject.class);
    }
}
