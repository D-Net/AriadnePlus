package eu.dnetlib.ariadneplus.elasticsearch.model;

import com.google.gson.Gson;
import org.apache.lucene.spatial3d.geom.GeoShape;
import org.elasticsearch.common.geo.GeoPoint;

import java.util.List;
import java.util.Objects;


public class Spatial {
    private String placeName;
    private String address;
    private GeoPoint geopoint;
    private String boundingbox;
    private String polygon;
    private String spatialPrecision;
    private String coordinatePrecision;
    private GeoPoint centroid;

    private transient String boundingBoxMaxLat;
    private transient String boundingBoxMaxLon;
    private transient String boundingBoxMinLat;
    private transient String boundingBoxMinLon;
    private transient String lat;
    private transient String lon;
    private transient List<AriadneGeoPoint> polygonGeoPoints;
    private transient String wkt;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBoundingBoxMaxLat() {
        return boundingBoxMaxLat;
    }

    public void setBoundingBoxMaxLat(String boundingBoxMaxLat) {
        this.boundingBoxMaxLat = boundingBoxMaxLat;
    }

    public String getBoundingBoxMaxLon() {
        return boundingBoxMaxLon;
    }

    public void setBoundingBoxMaxLon(String boundingBoxMaxLon) {
        this.boundingBoxMaxLon = boundingBoxMaxLon;
    }

    public String getBoundingBoxMinLat() {
        return boundingBoxMinLat;
    }

    public void setBoundingBoxMinLat(String boundingBoxMinLat) {
        this.boundingBoxMinLat = boundingBoxMinLat;
    }

    public String getBoundingBoxMinLon() {
        return boundingBoxMinLon;
    }

    public void setBoundingBoxMinLon(String boundingBoxMinLon) {
        this.boundingBoxMinLon = boundingBoxMinLon;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getSpatialPrecision() {
        return spatialPrecision;
    }

    public void setSpatialPrecision(String spatialPrecision) {
        this.spatialPrecision = spatialPrecision;
    }

    public String getCoordinatePrecision() {
        return coordinatePrecision;
    }

    public void setCoordinatePrecision(String coordinatePrecision) {
        this.coordinatePrecision = coordinatePrecision;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getBoundingbox() {
        return boundingbox;
    }

    public void setBoundingbox(String boundingbox) {
        this.boundingbox = boundingbox;
    }

    public GeoPoint getGeopoint() {
        return geopoint;
    }

    public String getPolygon() {
        return polygon;
    }

    public void setPolygon(String polygon) {
        this.polygon = polygon;
    }

    public void setGeopoint(GeoPoint geopoint) {
        this.geopoint = geopoint;
    }

    public List<AriadneGeoPoint> getPolygonGeoPoints() {
        return polygonGeoPoints;
    }

    public void setPolygonGeoPoints(List<AriadneGeoPoint> polygonGeoPoints) {
        this.polygonGeoPoints = polygonGeoPoints;
    }

    public String getWkt() {
        return wkt;
    }

    public void setWkt(String wkt) {
        this.wkt = wkt;
    }

    public GeoPoint getCentroid() {
        return centroid;
    }

    public void setCentroid(GeoPoint centroid) {
        this.centroid = centroid;
    }

    public Spatial() {
    }

    public static Spatial fromJson(String json){
        return new Gson().fromJson(json, Spatial.class);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Spatial)) return false;
        Spatial spatial = (Spatial) o;
        return Objects.equals(getPlaceName(), spatial.getPlaceName()) && Objects.equals(getAddress(), spatial.getAddress()) && Objects.equals(getGeopoint(), spatial.getGeopoint()) && Objects.equals(getBoundingbox(), spatial.getBoundingbox()) && Objects.equals(getPolygon(), spatial.getPolygon()) && Objects.equals(getSpatialPrecision(), spatial.getSpatialPrecision()) && Objects.equals(getCoordinatePrecision(), spatial.getCoordinatePrecision()) && Objects.equals(getCentroid(), spatial.getCentroid()) && Objects.equals(getBoundingBoxMaxLat(), spatial.getBoundingBoxMaxLat()) && Objects.equals(getBoundingBoxMaxLon(), spatial.getBoundingBoxMaxLon()) && Objects.equals(getBoundingBoxMinLat(), spatial.getBoundingBoxMinLat()) && Objects.equals(getBoundingBoxMinLon(), spatial.getBoundingBoxMinLon()) && Objects.equals(getLat(), spatial.getLat()) && Objects.equals(getLon(), spatial.getLon()) && Objects.equals(getPolygonGeoPoints(), spatial.getPolygonGeoPoints()) && Objects.equals(getWkt(), spatial.getWkt());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPlaceName(), getAddress(), getGeopoint(), getBoundingbox(), getPolygon(), getSpatialPrecision(), getCoordinatePrecision(), getCentroid(), getBoundingBoxMaxLat(), getBoundingBoxMaxLon(), getBoundingBoxMinLat(), getBoundingBoxMinLon(), getLat(), getLon(), getPolygonGeoPoints(), getWkt());
    }
}
