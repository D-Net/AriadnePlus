package eu.dnetlib.ariadneplus.elasticsearch.model;

import com.google.gson.Gson;

public class AriadneResource {
    private String label;
    private String uri;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public AriadneResource() {
    }

    public static AriadneResource fromJson(String json){
        return new Gson().fromJson(json, AriadneResource.class);
    }
}
