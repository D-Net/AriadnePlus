package eu.dnetlib.ariadneplus.elasticsearch.model;

import com.google.gson.Gson;

public class CountryInfo {
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CountryInfo() {
    }

    public static CountryInfo fromJson(String json){
        return new Gson().fromJson(json, CountryInfo.class);
    }
}
