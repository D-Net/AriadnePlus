package eu.dnetlib.ariadneplus.elasticsearch.model;

import com.google.gson.Gson;
import org.apache.commons.text.WordUtils;

public class NativePeriod {
    private String from;
    private String periodName;
    private String until;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getPeriodName() {
        return periodName;
    }

    public void setPeriodName(String periodName) {
        this.periodName = periodName.toLowerCase();
    }

    public String getUntil() {
        return until;
    }

    public void setUntil(String until) {
        this.until = until;
    }

    public NativePeriod() {
    }

    public static NativePeriod fromJson(String json){
        return new Gson().fromJson(json, NativePeriod.class);
    }

}
