package eu.dnetlib.ariadneplus.elasticsearch.model;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import org.apache.commons.text.WordUtils;

import java.util.Map;

public class Temporal {
    private String from;
    private String periodName;
    private String until;
    private String uri;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getPeriodName() {
        return periodName;
    }

    public void setPeriodName(String periodName) {
        this.periodName = periodName.toLowerCase();
    }

    public String getUntil() {
        return until;
    }

    public void setUntil(String until) {
        this.until = until;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Temporal() {
    }

    public static Temporal fromJson(String json){
        return new Gson().fromJson(json, Temporal.class);
    }
}
