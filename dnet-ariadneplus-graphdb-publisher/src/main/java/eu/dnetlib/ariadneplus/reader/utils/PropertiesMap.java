package eu.dnetlib.ariadneplus.reader.utils;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

@Component
public class PropertiesMap {

    @Value("${class.map.specifications:undefined}")
    private String spec;

    private static Map<String, ClassSpec> map;

    public ClassSpec get(String key){
        return map.get(key);
    }

    public  void fill(String spec){
        Type mapType = new TypeToken<HashMap<String,ClassSpec>>(){}.getType();
        map = new Gson().fromJson(spec,mapType);

    }

    @PostConstruct
    public  void fill(){
        Type mapType = new TypeToken<HashMap<String,ClassSpec>>(){}.getType();
        map = new Gson().fromJson(spec,mapType);

    }
}
