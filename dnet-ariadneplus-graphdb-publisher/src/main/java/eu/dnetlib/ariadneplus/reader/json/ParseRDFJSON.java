package eu.dnetlib.ariadneplus.reader.json;

import java.util.Iterator;
import java.util.LinkedHashMap;

import eu.dnetlib.ariadneplus.reader.RunSPARQLQueryService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;


@Service
public class ParseRDFJSON {

    private static final Log log = LogFactory.getLog(ParseRDFJSON.class);

    static JSONObject map ;

    @Value("${catalog.entry.path}")
    private  String catalogEntryJsonPath;

    @Value("${catalog.entry.collection.path}")
    private  String catalogEntryCollectionJsonPath;

    private boolean isCollection = false;

    private String json;

    private Iterator<Object> it ;

    public String getJson() {
        return json;
    }


    public void setJson(String json) throws ParseException {
        this.json = json;
    }

    private void fillMap() throws ParseException {
        map = (JSONObject)(new JSONParser(JSONParser.MODE_PERMISSIVE).parse(json));

    }

    public int parse(String json) throws ParseException {
        setJson(json);
        fillMap();
        DocumentContext jsonContext = JsonPath.parse(json);
//        log.debug("jsonPath: "+getCatalogEntryJsonPath());
//        log.debug("json from jsonContext: "+json);
        JSONArray entries = jsonContext.read(getCatalogEntryJsonPath());
        int size = entries.size();
        if (size==0) {
            return -1;
        }
        it = entries.iterator();
        return size;
    }

    public boolean hasNextElement(){
        if(it == null) return false;
        return it.hasNext();
    }

    public LinkedHashMap getNextElement(){
        return (LinkedHashMap)it.next();
    }

    public static JSONObject get(String key){
        return (JSONObject) map.get(key);
    }

    public String getCatalogEntryJsonPath() {
        if (isCollection) {
            return catalogEntryCollectionJsonPath;
        }
        return catalogEntryJsonPath;
    }

    public void setCatalogEntryJsonPath(String catalogEntryJsonPath) {
        this.catalogEntryJsonPath = catalogEntryJsonPath;
    }

    public void setCatalogEntryCollectionJsonPath(String catalogEntryCollectionJsonPath) {
        this.catalogEntryCollectionJsonPath = catalogEntryCollectionJsonPath;
    }

    public boolean isCollection() {
        return isCollection;
    }

    public void setCollection(boolean collection) {
        isCollection = collection;
    }
}
