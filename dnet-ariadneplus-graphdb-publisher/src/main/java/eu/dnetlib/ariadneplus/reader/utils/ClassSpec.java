package eu.dnetlib.ariadneplus.reader.utils;

import com.google.gson.Gson;

import java.util.List;
import java.util.Map;

public class ClassSpec {
    private  String class_type;
    private Map<String,Mappings> mappings;

    public ClassSpec() {
    }


    public  String getClass_type() {
        return class_type;
    }

    public  void setClass_type(String class_type) {
        this.class_type = class_type;
    }

    public Map<String, Mappings> getMappings() {
        return mappings;
    }

    public void setMappings(Map<String, Mappings> mappings) {
        this.mappings = mappings;
    }

    public static ClassSpec fromJson(String json){
        return new Gson().fromJson(json, ClassSpec.class);
    }

    public String toJson(){
        return new Gson().toJson(this);
    }


    public Mappings get(String predicate) {
        return mappings.get(predicate);
    }
}
