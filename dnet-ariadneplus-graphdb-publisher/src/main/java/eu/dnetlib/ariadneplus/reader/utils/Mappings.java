package eu.dnetlib.ariadneplus.reader.utils;

import com.google.gson.Gson;

public class Mappings {
    private String class_field;
    private String substring;
    private String element_type;
    private String external_reference;

    public Mappings() {
    }


    public String getClass_field() {
        return class_field;
    }

    public void setClass_field(String class_field) {
        this.class_field = class_field;
    }

    public String getSubstring() {
        return substring;
    }

    public void setSubstring(String substring) {
        this.substring = substring;
    }

    public String getElement_type() {
        return element_type;
    }

    public void setElement_type(String element_type) {
        this.element_type = element_type;
    }

    public String getExternal_reference() {
        return external_reference;
    }

    public void setExternal_reference(String external_reference) {
        this.external_reference = external_reference;
    }

    public static Mappings fromJson(String json){
        return new Gson().fromJson(json, Mappings.class);
    }

    public String toJson(){
        return new Gson().toJson(this);
    }

    public boolean hasExternalReference() {
        return external_reference != null ;
    }
}
