package eu.dnetlib.ariadneplus.reader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.IOException;

@Service
public class RunSPARQLWriteService {

	private static final Log log = LogFactory.getLog(RunSPARQLWriteService.class);

    private RemoteRepositoryManager manager;
    private Repository repository;
    private ValueFactory valueFactory;

	private static String username = null;
	private static String pwd = null;
	private static String graphDBUrl = null;
	private static String graphDBRepository = null;

	@PreDestroy
	public void preDestroy() {
		shutDownRepository();
	}

	public void setupConnection(String username, String pwd, String graphDbUrl, String graphDbRepository) {
		setUsername(username);
		setPwd(pwd);
		setGraphDBUrl(graphDbUrl);
		setGraphDBRepository(graphDbRepository);
		initRepository();
	}

	private void initRepository(){
		if (manager==null) {
			manager = new RemoteRepositoryManager(getGraphDBUrl());
			manager.init();
			manager.setUsernameAndPassword(getUsername(), getPwd());
			repository = manager.getRepository(getGraphDBRepository());
			valueFactory = repository.getValueFactory();
		}
    }

    private void shutDownRepository(){
        if (repository!=null) {
			repository.shutDown();
		}
		if (manager!=null) {
			manager.shutDown();
		}
    }

	public static String getUsername() {
		return username;
	}

	public static String getPwd() {
		return pwd;
	}

	public static String getGraphDBUrl() {
		return graphDBUrl;
	}

	public static String getGraphDBRepository() {
		return graphDBRepository;
	}

	public static void setUsername(String username) {
		RunSPARQLWriteService.username = username;
	}

	public static void setPwd(String pwd) {
		RunSPARQLWriteService.pwd = pwd;
	}

	public static void setGraphDBUrl(String graphDBUrl) {
		RunSPARQLWriteService.graphDBUrl = graphDBUrl;
	}

	public static void setGraphDBRepository(String graphDBRepository) {
		RunSPARQLWriteService.graphDBRepository = graphDBRepository;
	}

	public long writeRecord(java.io.InputStream rdfxmlRecord, String recordURI, String datasourceApi, String graphDBBaseURI){
		RepositoryConnection connection = repository.getConnection();
		try {
//			connection.begin(); currently with rdf4j 3.6.3 the transaction throws OOM
			connection.add(
					rdfxmlRecord,
					recordURI,
					RDFFormat.RDFXML,
					valueFactory.createIRI(graphDBBaseURI, datasourceApi));
//			connection.commit();
		} catch(Exception e){
			log.error(String.format("Exception processing result with URI %s:\n%s",recordURI, e));
			return -1;
		} finally{
			if (connection!=null && connection.isOpen()) {
				connection.close();
			}
			try {
				rdfxmlRecord.close();
			} catch (IOException e) {
				log.error(e);
			}
		}
		return 0;
	}
}
