package eu.dnetlib.ariadneplus.reader.utils;

import com.github.sisyphsu.dateparser.DateParser;
import com.github.sisyphsu.dateparser.DateParserUtils;
import eu.dnetlib.ariadneplus.elasticsearch.BulkUpload;
import eu.dnetlib.ariadneplus.elasticsearch.model.AriadnePlusEntry;
import eu.dnetlib.ariadneplus.elasticsearch.model.AriadneSubject;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.Year;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.format.TextStyle;
import java.time.temporal.ChronoField;
import java.util.Collections;
import java.util.Locale;
import java.util.function.Predicate;

public class ESUtils {
    private static final Log log = LogFactory.getLog(ESUtils.class);
    private static String elasticSearchDateFormat = "yyyy-MM-dd";


    public static String getESFormatDate(String originalDate) {
            if (StringUtils.isBlank(originalDate)) {
                return null;
            }
            String inputDate = originalDate.trim();
            // the library completes with 01-01 if we hae only the year: we do not want that.
            if(inputDate.length() == 4 && StringUtils.isNumeric(inputDate)){
                return inputDate;
            }
            try {
                final LocalDate date = DateParserUtils
                        .parseDate(inputDate.trim())
                        .toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();
                return DateTimeFormatter.ofPattern(elasticSearchDateFormat).format(date);
            } catch (DateTimeParseException e) {
                return null;
            }
    }


    public static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public static String getIndexId(String identifier) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] encoded_hash = digest.digest(
                identifier.getBytes(StandardCharsets.UTF_8));
        return ESUtils.bytesToHex(encoded_hash);
    }

    public static boolean hasMandatoryFields(AriadnePlusEntry ace, boolean isCollection){
        boolean passed = true;
        if(StringUtils.isBlank(ace.getAccessRights())) {
            log.error(String.format("%s lacks access rights", ace.getIdentifier()));
            passed= false;
        }
        if(ace.getAriadneSubject() == null || ace.getAriadneSubject().isEmpty() || ace.getAriadneSubject().stream().allMatch(ariadneSubject -> ariadneSubject == null || StringUtils.isBlank(ariadneSubject.getPrefLabel()))) {
            log.error(String.format("%s lacks ARIADNE Subject", ace.getIdentifier()));
            passed= false;
        }
        if(ace.getPublisher() == null || ace.getPublisher().isEmpty() || ace.getPublisher().stream().allMatch(publisher -> publisher == null || StringUtils.isBlank(publisher.getName()))) {
            log.error(String.format("%s lacks publisher", ace.getIdentifier()));
            passed= false;
        }
        if(ace.getOwner() == null || ace.getOwner().isEmpty() || ace.getOwner().stream().allMatch(owner -> owner == null || StringUtils.isBlank(owner.getName()))) {
            log.error(String.format("%s lacks owner", ace.getIdentifier()));
            passed= false;
        }
        if(ace.getResponsible() == null || ace.getResponsible().isEmpty() || ace.getResponsible().stream().allMatch(resp -> resp == null || StringUtils.isBlank(resp.getName()))) {
            log.error(String.format("%s lacks responsible", ace.getIdentifier()));
            passed= false;
        }
        if(ace.getContributor() == null || ace.getContributor().isEmpty() || ace.getContributor().stream().allMatch(contributor -> contributor == null || StringUtils.isBlank(contributor.getName()))) {
            log.error(String.format("%s lacks contributor", ace.getIdentifier()));
            passed= false;
        }
        if(ace.getCreator() == null || ace.getCreator().isEmpty() || ace.getCreator().stream().allMatch(creator -> creator == null || StringUtils.isBlank(creator.getName()))) {
            log.error(String.format("%s lacks creator", ace.getIdentifier()));
            passed= false;
        }
        if(StringUtils.isBlank(ace.getIssued())) {
            log.error(String.format("%s lacks issued date", ace.getIdentifier()));
            passed= false;
        }
        if(StringUtils.isBlank(ace.getModified())) {
            log.error(String.format("%s lacks modified date", ace.getIdentifier()));
            passed= false;
        }
        if(StringUtils.isBlank(ace.getOriginalId())) {
            log.error(String.format("%s lacks original id", ace.getIdentifier()));
            passed= false;
        }
        if(ace.getTitle() == null || ace.getTitle().isEmpty() || ace.getTitle().stream().allMatch(title -> title == null || StringUtils.isBlank(title.getText()))) {
            log.error(String.format("%s lacks title", ace.getIdentifier()));
            passed= false;
        }
        if(ace.getHas_type() == null || StringUtils.isBlank(ace.getHas_type().getLabel()) || StringUtils.isBlank(ace.getHas_type().getUri())) {
            log.error(String.format("%s lacks has_type", ace.getIdentifier()));
            passed= false;
        }
        if(!isCollection && (ace.getIsPartOf() == null || ace.getIsPartOf().isEmpty() || ace.getIsPartOf().stream().allMatch(StringUtils::isBlank))) {
            log.error(String.format("%s lacks link to collection", ace.getIdentifier()));
            passed= false;
        }
        if(!isCollection && (ace.getNativeSubject() == null || ace.getNativeSubject().isEmpty() || ace.getNativeSubject().stream().allMatch(subj -> subj == null || StringUtils.isBlank(subj.getPrefLabel())))) {
            log.error(String.format("%s lacks native subject", ace.getIdentifier()));
            passed= false;
        }
        return passed;
    }

}
