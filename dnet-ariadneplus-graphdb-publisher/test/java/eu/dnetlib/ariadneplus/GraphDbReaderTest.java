/**
 * 
 */
package eu.dnetlib.ariadneplus;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.rdf4j.http.protocol.UnauthorizedException;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.query.GraphQuery;
import org.eclipse.rdf4j.query.GraphQueryResult;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

@Ignore
public class GraphDbReaderTest {

    private static final Log log = LogFactory.getLog(GraphDbReaderTest.class);

    private static String username = "reader";
    private static String pwd = "***";
    private static String graphDBUrl = "https://graphdb-test.ariadne.d4science.org";
    private static String graphDBRepository = "ariadneplus-ts01";
    private RepositoryConnection connection;
    private RemoteRepositoryManager manager;
    private Repository repository;

    @Test
    public void readADS398CollectionTest() throws Exception {
        String resourceId = "https://ariadne-infrastructure.eu/aocat/Collection/ADS/AAA81A6D-56F3-341C-BAF0-791C31BC7F73";
        String datasource = "ads";
        String collectionId = "398";
        String result = read(resourceId, datasource, collectionId);
        log.info(result);
    }

    private String read(String resourceId, String datasource, String collectionId) throws Exception {
        final ClassPathResource queryTemplateResource;
        queryTemplateResource = new ClassPathResource("eu/dnetlib/ariadneplus/sparql/read_collection_data_template.sparql");
        String queryTemplate = IOUtils.toString(queryTemplateResource.getInputStream(), StandardCharsets.UTF_8.name());
        if (queryTemplate==null)
            throw new Exception("Query template not found.");
        final String selectQueryTemplate = queryTemplate.replaceAll("%datasource", datasource).replaceAll("%collectionId", collectionId);
        String query = selectQueryTemplate.replaceAll("%record", "<"+resourceId+">");
        return executeQueryGraph(query);
    }

    private String executeQueryGraph(String query) throws Exception {
        if (!openConnection()) {
            throw new Exception("User not authorized.");
        };
        StringWriter recordWriter = null;
        Model resultsModel = null;
        try {
            GraphQuery graphQuery = connection.prepareGraphQuery(QueryLanguage.SPARQL, query);
            GraphQueryResult graphQueryResult = graphQuery.evaluate();
            resultsModel = QueryResults.asModel(graphQueryResult);
            graphQueryResult.close();
            if (resultsModel.size()==0) {
                throw new Exception("No result.");
            }
            recordWriter = new StringWriter();
            RDFWriter rdfRecordWriter = Rio.createWriter(RDFFormat.RDFJSON, recordWriter);
            Rio.write(resultsModel, rdfRecordWriter);
            return recordWriter.toString();
        } catch(Exception e){
            log.error(e);
            throw new Exception(e);
        } finally{
            closeConnection();
            if (resultsModel!=null) {
                resultsModel.clear();
            }
        }
    }

    private boolean openConnection() throws Exception {

        manager = new RemoteRepositoryManager(getGraphDBUrl());
        manager.init();
       // manager.setUsernameAndPassword(getUsername(), getPwd());
        try {
            repository = manager.getRepository(getGraphDBRepository());
        } catch (UnauthorizedException e) {
            return false;
        }
        connection =  repository.getConnection();
        return true;
    }

    private void closeConnection(){
        connection.close();
        repository.shutDown();
        manager.shutDown();
    }

    private static String getUsername() {
        return username;
    }

    private static String getPwd() {
        return pwd;
    }

    private static String getGraphDBUrl() {
        return graphDBUrl;
    }

    private static String getGraphDBRepository() {
        return graphDBRepository;
    }
}
