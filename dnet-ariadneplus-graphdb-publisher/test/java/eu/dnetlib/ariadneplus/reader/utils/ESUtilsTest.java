package eu.dnetlib.ariadneplus.reader.utils;

import org.junit.Assert;
import org.junit.Test;

public class ESUtilsTest {

//    Elastic search format: yyyy-MM-dd or yyyy

    @Test
    public void testParseDate(){
        String date = "2013-03-13";
        String parsed =  ESUtils.getESFormatDate(date);
        Assert.assertEquals(parsed, date);
    }

    @Test
    public void testBSTDate(){
        String date = "Fri May 15 13:21:02 BST 2020";
        String parsed =  ESUtils.getESFormatDate(date);
        Assert.assertEquals("2020-05-15", parsed);
    }


    @Test
    public void testYearDate(){
        String date = "2012";
        String parsed =  ESUtils.getESFormatDate(date);
        Assert.assertEquals("2012", parsed);
    }

    @Test
    public void testEmtyDate(){
        String date = "";
        String parsed =  ESUtils.getESFormatDate(date);
        Assert.assertEquals("0000", parsed);
    }

    @Test
    public void testErrorDate(){
        String date = "????";
        String parsed =  ESUtils.getESFormatDate(date);
        Assert.assertEquals("0000", parsed);
    }
    @Test
    public void testUnknownDate(){
        String date = "unknown";
        String parsed =  ESUtils.getESFormatDate(date);
        Assert.assertEquals("0000", parsed);
    }


    @Test
    public void testDateMonthString(){
        String date = "27-Oct-2022";
        String parsed =  ESUtils.getESFormatDate(date);
        System.out.println(parsed);
        Assert.assertEquals("2022-10-27", parsed);
    }
    @Test
    public void testDateCapitalMonthString(){
        String date = "27-OCT-2022";
        String parsed =  ESUtils.getESFormatDate(date);
        System.out.println(parsed);
        Assert.assertEquals("2022-10-27", parsed);
    }

    @Test
    public void testDateMonthString2(){
        String date = "27 Oct 2022";
        String parsed =  ESUtils.getESFormatDate(date);
        System.out.println(parsed);
        Assert.assertEquals("2022-10-27", parsed);
    }




}
