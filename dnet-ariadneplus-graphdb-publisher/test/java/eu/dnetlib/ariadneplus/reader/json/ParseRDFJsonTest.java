package eu.dnetlib.ariadneplus.reader.json;

import eu.dnetlib.ariadneplus.elasticsearch.model.AriadnePlusEntry;
import eu.dnetlib.ariadneplus.reader.ResourceManager;
import net.minidev.json.parser.ParseException;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;

public class ParseRDFJsonTest {

    ParseRDFJSON parser;
    ResourceManager mng;
    String spec = getFromClasspath("eu/dnetlib/ariadneplus/reader/json/spec.json");

    @Before
    public  void setup(){
        parser = new ParseRDFJSON();
        parser.setCatalogEntryCollectionJsonPath("$[*][?(@['https://www.ariadne-infrastructure.eu/property/rdfType'][0]['value']=='Collection')]");
        parser.setCatalogEntryJsonPath("[*][?(@['https://www.ariadne-infrastructure.eu/property/rdfType'][0]['value']=='Record')]");
        mng = new ResourceManager();
        mng.setup("https://www.ariadne-infrastructure.eu/property/resourceType", "eu.dnetlib.ariadneplus.elasticsearch.model.",
                "[\"https://www.ariadne-infrastructure.eu/property/resourceType\", \"http://www.w3.org/1999/02/22-rdf-syntax-ns#type\"]",
                spec);
    }

    @Test
    public void testParseCollectionJson() throws ParseException {
        String collectionJson = getFromClasspath("eu/dnetlib/ariadneplus/reader/json/collection.json");
        parser.setCollection(true);
        int res = parser.parse(collectionJson);
        Assert.assertNotEquals(-1, res);
        while(parser.hasNextElement()){
            System.out.println(parser.getNextElement());
        }
    }

    @Test
    public void testParseRecord() throws ParseException {
        String recordJson = getFromClasspath("eu/dnetlib/ariadneplus/reader/json/test_cenieh.json");
        parser.setCollection(false);
        int res = parser.parse(recordJson);
        Assert.assertNotEquals(-1, res);
        while(parser.hasNextElement()){
            System.out.println(parser.getNextElement());
        }
    }

    @Test
    public void testParseCNRS() throws ParseException {
        String recordJson = getFromClasspath("eu/dnetlib/ariadneplus/reader/json/test_cnrs.json");
        parser.setCollection(false);
        int res = parser.parse(recordJson);
        Assert.assertNotEquals(-1, res);
        while(parser.hasNextElement()){
            System.out.println(parser.getNextElement());
        }
    }

    @Test
    public void testParseUKPool() throws ParseException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        String recordJson = getFromClasspath("eu/dnetlib/ariadneplus/reader/json/ukpool-coll.json");
        parser.setCollection(true);
        int res = parser.parse(recordJson);
        Assert.assertNotEquals(-1, res);
//        while(parser.hasNextElement()){
//            System.out.println(parser.getNextElement());
//        }
        mng.manage(parser);
        Object next = mng.next();
        AriadnePlusEntry ace = ((AriadnePlusEntry) next);
        Assert.assertNotNull(ace);
        ace.getTemporal().stream().forEach(e -> System.out.println(e.getUri()));

        System.out.println(ace.toJson());
    }


    @Test
    public void testParseEmptyJson() throws ParseException {
        String collectionJson = "{}";
        parser.setCollection(true);
        parser.parse(collectionJson);
        int res = parser.parse(collectionJson);
        Assert.assertEquals(-1, res);
        while(parser.hasNextElement()){
            System.out.println(parser.getNextElement());
        }
    }

    @Test
    public void testResourceManager() throws ParseException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        String collectionJson = getFromClasspath("eu/dnetlib/ariadneplus/reader/json/collection.json");
        parser.setCollection(true);
        int res = parser.parse(collectionJson);
        Assert.assertNotEquals(-1, res);
        mng.manage(parser);
        if(mng.hasNext())
            System.out.println(mng.next());
    }

    @Test
    public void testParseThanados() throws ParseException {
        String recordJson = getFromClasspath("eu/dnetlib/ariadneplus/reader/json/test-thanados.json");
        parser.setCollection(true);
        int res = parser.parse(recordJson);
        Assert.assertNotEquals(-1, res);
        while(parser.hasNextElement()){
            System.out.println(parser.getNextElement());
        }
    }

    @Test
    public void testParseRoceeh() throws ParseException {
        String recordJson = getFromClasspath("eu/dnetlib/ariadneplus/reader/json/roceeh.json");
        parser.setCollection(false);
        int res = parser.parse(recordJson);
        Assert.assertNotEquals(-1, res);
        while(parser.hasNextElement()){
            System.out.println(parser.getNextElement());
        }
    }

    @Test
    public void testCountryInCollection() throws ParseException {
        String collectionJson = getFromClasspath("eu/dnetlib/ariadneplus/reader/json/collection.json");
        parser.setCollection(true);
        int res = parser.parse(collectionJson);
        Assert.assertNotEquals(-1, res);
        boolean hasCountry = false;
        while(parser.hasNextElement()){
            LinkedHashMap element = parser.getNextElement();
            System.out.println(element);
            hasCountry = element.containsKey("https://www.ariadne-infrastructure.eu/property/country");
        }
        Assert.assertTrue(hasCountry);

    }

    private String getFromClasspath(String s) {
        try {
            final ClassPathResource resource = new ClassPathResource(s);
            return IOUtils.toString(resource.getInputStream(), "UTF-8");
        }catch(IOException e){
            return null;
        }
    }
}
