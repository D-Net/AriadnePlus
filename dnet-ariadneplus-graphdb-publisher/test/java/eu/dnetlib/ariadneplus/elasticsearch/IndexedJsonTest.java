package eu.dnetlib.ariadneplus.elasticsearch;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;

public class IndexedJsonTest {

    @Test
    public void testSimple() throws JSONException {
        String old = getJSONResult("eu/dnetlib/ariadneplus/elasticsearch/old.json");
        String the_new = getJSONResult("eu/dnetlib/ariadneplus/elasticsearch/new.json");
        JSONAssert.assertEquals(old, the_new, false); // Pass
        //JSONAssert.assertEquals(old, the_new, true); // Fail
    }

    private String getJSONResult(String s) {
        try {
            final ClassPathResource resource = new ClassPathResource(s);
            return IOUtils.toString(resource.getInputStream(), "UTF-8");
        }catch(IOException e){
            return null;
        }
    }
}
